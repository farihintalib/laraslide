(function ($, window) {
    "use strict";
    window.chart = function () {
        var line = function (el) {
            $.ajax({
                url: "/charts?type=line",
                method: "GET",
                success: function (response) {
                    var options = {
                        series: [{
                                name: "Desktops",
                                data: response.data
                            }],
                        chart: {
                            height: 350,
                            type: 'line',
                            zoom: {
                                enabled: false
                            }
                        },
                        dataLabels: {
                            enabled: false
                        },
                        stroke: {
                            curve: 'straight'
                        },
                        title: {
                            text: 'Product Trends by Month',
                            align: 'left'
                        },
                        grid: {
                            row: {
                                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                                opacity: 0.5
                            },
                        },
                        xaxis: {
                            categories: response.categories
                        }
                    };
                    var chart = new ApexCharts(document.querySelector(el), options);
                    chart.render();
                }
            });
        };
        var area = function (el) {
            $.ajax({
                url: "/charts?type=area",
                method: "GET",
                success: function (response) {
                    var options = {
                        series: response.series,
                        chart: {
                            height: 350,
                            type: 'area'
                        },
                        dataLabels: {
                            enabled: false
                        },
                        stroke: {
                            curve: 'smooth'
                        },
                        xaxis: {
                            type: 'datetime',
                            categories: ["2018-09-19T00:00:00.000Z", "2018-09-19T01:30:00.000Z", "2018-09-19T02:30:00.000Z", "2018-09-19T03:30:00.000Z", "2018-09-19T04:30:00.000Z", "2018-09-19T05:30:00.000Z", "2018-09-19T06:30:00.000Z"]
                        },
                        tooltip: {
                            x: {
                                format: 'dd/MM/yy HH:mm'
                            },
                        },
                    };
                    var chart = new ApexCharts(document.querySelector(el), options);
                    chart.render();
                }
            });
        };
        return {
            line: function (el) {
                line(el);
            },
            area: function (el) {
                area(el);
            }
        };
    }();
    chart.line('#line');
    chart.area('#area');
})(jQuery, window);