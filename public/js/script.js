(function ($, window, document) {
    'use strict';

    window.upsert = function (data) {
        var swalSettings = window[data.swalId + 'Settings'];
        Swal.fire({
            title: swalSettings.title,
            icon: swalSettings.icon,
            html: swalSettings.html,
            showCancelButton: swalSettings.showCancelButton,
            showConfirmButton: swalSettings.showConfirmButton,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: swalSettings.confirmButtonText,
            showLoaderOnConfirm: true,
            allowOutsideClick: () => !Swal.isLoading(),
            customClass: {
                popup: swalSettings.customClass.popup,
                content: 'text-left'
            },
            onOpen: () => {
                $("form#" + data.swalId + " .select2").select2({
                    allowClear: true,
                    dropdownParent: $('.swal2-modal')
                });
                $(".swal2-modal .select2-hidden-accessible").attr('style', 'position: inherit !important;');
                $(".swal2-modal .select2-container").attr('style', 'width: 100%;font-size: 1rem;text-align: initial;');
                swalSettings.onOpen(data);
            },
            preConfirm: () => {
                var form = $("form#" + data.swalId);
                var validate = form[0].checkValidity();
                form[0].reportValidity();
                var formData = new FormData(form[0]);
                if (validate) {
                    let config = {
                        headers: {
                            Accept: swalSettings.axios.headersAccept
                        }
                    };
                    swalSettings.axios.afterValidate(data);
                    return axios.post(swalSettings.axios.url(data), formData, config)
                            .then(response => {
                                if (response.status === 200) {
                                    let timer = 1000;
                                    if (response.data.timer === 'false') {
                                        timer = 0;
                                    }
                                    Swal.fire({
                                        title: response.data.title,
                                        text: response.data.message,
                                        icon: response.data.type,
                                        timer: timer
                                    }).then(() => {
                                        if (response.data.callback === 'redirect') {
                                            window.location = response.data.url;
                                        } else {
                                            $.each(window.LaravelDataTables, function (key, value) {
                                                window.LaravelDataTables[key].ajax.reload(null, false);
                                            });
                                        }
                                        swalSettings.axios.afterSuccessResponse(data);
                                    });
                                } else {
                                    Swal.showValidationMessage(response.status + ": " + response.data.message);
                                }
                            }).catch(error => {
                        if (error.response.status === 422) {
                            let responseError = '';
                            var responseErrors = $.map(error.response.data.errors, function (value, index) {
                                return [value[0]];
                            });
                            responseErrors.forEach(function (item, index) {
                                responseError += item + "<br>";
                            });
                            Swal.showValidationMessage(responseError);
                        } else {
                            Swal.showValidationMessage(error.response.status + ": " + error.response.data.message);
                        }
                    });
                } else {
                    return false;
                }
            }
        });
    };

    window.proceed = function (data) {
        var swalSettings = window[data.swalId + 'Settings'] || null;
        if (data.route) {
            Swal.fire({
                title: data.title,
                text: data.message,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !Swal.isLoading(),
                preConfirm: () => {
                    return axios({
                        method: data.method,
                        url: data.route,
                        headers: {
                            Accept: data.accept,
                        },
                        params: data.params,
                    }).then(response => {
                        if (response.status === 200) {
                            let timer = 1000;
                            if (response.data.timer == 'false') {
                                timer = 0;
                            }
                            Swal.fire({
                                title: response.data.title,
                                text: response.data.message,
                                icon: response.data.type,
                                timer: timer,
                            }).then(() => {
                                if (response.data.callback == 'redirect') {
                                    window.location = response.data.url;
                                } else {
                                    $.each(window.LaravelDataTables, function (key, value) {
                                        window.LaravelDataTables[key].ajax.reload(null, false);
                                    });
                                }
                            });
                            swalSettings.axios.afterSuccessResponse(data);
                        } else {
                            Swal.showValidationMessage(response.status + ": " + response.data.message);
                        }
                    }).catch(error => {
                        Swal.showValidationMessage(error.response.status + ": " + error.response.data.message);
                    });
                },
            });
        } else {
            Swal.fire({
                title: data.title,
                text: data.message,
                icon: 'info',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ok',
            });
        }
    };
})(jQuery, window, document);
