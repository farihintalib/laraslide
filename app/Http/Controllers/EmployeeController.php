<?php

/**
 * Generate at 2021-03-28 02:41:06. Clear comment if this file being used
 */

namespace App\Http\Controllers;

use App\DataTables\Employee\EmployeeDt;
use App\Http\Requests\Employee\StoreRequest;
use App\Http\Requests\Employee\UpdateRequest;
use App\Models\Employee;
use App\Models\Company;

class EmployeeController extends Controller
{
    
    protected $employeeModel;
    
    public function __construct() {
        $this->employeeModel = new Employee();
    }
    
    public function index()
    {
        $title = __('employee');
        if (!empty(request()->get('table'))) {
            return $this->dataTable();
        }
        $companies = Company::get()->pluck('name','id');
        $companies = Company::get()->map(function($instance){
            $instance->name = "{$instance->name} ({$instance->email})";
            return $instance;
        })->pluck('name','id');
        $compact = array_merge($this->dataTable(), compact('title','companies'));
        return view('employee.index', $compact);
    }

    public function dataTable() {
        $employeeDt = new EmployeeDt();
        if (request()->get('table') == 'employeeDt') {
            return $employeeDt->render(null);
        }
        return compact('employeeDt');
    }
    
    public function store(StoreRequest $request)
    {
        $input = request()->all();
        $input['created_by'] = auth()->user()->id ?? null;
        $this->employeeModel->create($input);
        return response()->json(['type' => 'success', 'title' => 'Created!', 'message' => 'Successfully Created']);
    }

    public function update(UpdateRequest $request, $id)
    {
        $employee = $this->employeeModel->findOrFail($id);
        $input = request()->all();
        $input['updated_by'] = auth()->user()->id ?? null;
        $employee->updateOrCreate(['id' => $id], $input);
        return response()->json(['type' => 'success', 'title' => 'Updated!', 'message' => 'Successfully Updated']);
    }

    /**
     * Remove the specified resource from storage.
     * @param type $id
     * @return response
     */
    public function destroy($id)
    {
        $employee = $this->employeeModel->findOrFail($id);
        $employee->delete();
        return response()->json(['type' => 'success', 'title' => 'Deleted!', 'message' => 'Successfully Deleted']);
    }
    
}
