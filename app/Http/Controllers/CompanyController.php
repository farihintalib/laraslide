<?php

/**
 * Generate at 2021-03-28 02:40:12. Clear comment if this file being used
 */

namespace App\Http\Controllers;

use App\DataTables\Company\CompanyDt;
use App\Http\Requests\Company\StoreRequest;
use App\Http\Requests\Company\UpdateRequest;
use App\Models\Company;

class CompanyController extends Controller
{
    
    protected $companyModel;
    
    public function __construct() {
        $this->companyModel = new Company();
    }
    
    public function index()
    {
        $title = __('company');
        if (!empty(request()->get('table'))) {
            return $this->dataTable();
        }
        $compact = array_merge($this->dataTable(), compact('title'));
        return view('company.index', $compact);
    }

    public function dataTable() {
        $companyDt = new CompanyDt();
        if (request()->get('table') == 'companyDt') {
            return $companyDt->render(null);
        }
        return compact('companyDt');
    }
    
    public function store(StoreRequest $request)
    {
        $input = request()->all();
        $input['created_by'] = auth()->user()->id ?? null;
        $this->companyModel->create($input);
        return response()->json(['type' => 'success', 'title' => 'Created!', 'message' => 'Successfully Created']);
    }

    public function update(UpdateRequest $request, $id)
    {
        $company = $this->companyModel->findOrFail($id);
        $input = request()->all();
        $input['updated_by'] = auth()->user()->id ?? null;
        $company->updateOrCreate(['id' => $id], $input);
        return response()->json(['type' => 'success', 'title' => 'Updated!', 'message' => 'Successfully Updated']);
    }

    /**
     * Remove the specified resource from storage.
     * @param type $id
     * @return response
     */
    public function destroy($id)
    {
        $company = $this->companyModel->findOrFail($id);
        $company->delete();
        return response()->json(['type' => 'success', 'title' => 'Deleted!', 'message' => 'Successfully Deleted']);
    }
    
}
