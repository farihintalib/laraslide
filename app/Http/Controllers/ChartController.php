<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChartController extends Controller {

    public function index(Request $request) {
        if ($request->ajax()) {
            return response($this->data($request->type));
        }
        return view('chart.index');
    }

    protected function data($type) {
        switch ($type) {
            case "line":
                return [
                    'data' => [10, 41, 35, 51, 49, 62, 69, 91, 100],
                    'categories' => ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep']
                ];
            case "area":
                return [
                    'series' => [
                        [
                            'name' => 'series1',
                            'data' => [31, 40, 28, 51, 42, 109, 100],
                        ],
                        [
                            'name' => 'series2',
                            'data' => [11, 32, 45, 32, 34, 52, 41]
                        ],
                    ],
                ];
        }
    }

}
