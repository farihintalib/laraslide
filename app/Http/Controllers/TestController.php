<?php

/**
 * Generate at 2021-03-26 17:11:12. Clear comment if this file being used
 */

namespace App\Http\Controllers;

use App\DataTables\Test\TestDt;
use App\Http\Requests\Test\StoreRequest;
use App\Http\Requests\Test\UpdateRequest;
use App\Models\Test;

class TestController extends Controller
{
    
    protected $testModel;
    
    public function __construct() {
        $this->testModel = new Test();
    }
    
    public function index()
    {
        $title = __('test');
        if (!empty(request()->get('table'))) {
            return $this->dataTable();
        }
        $compact = array_merge($this->dataTable(), compact('title'));
        return view('test.index', $compact);
    }

    public function dataTable() {
        $testDt = new TestDt();
        if (request()->get('table') == 'testDt') {
            return $testDt->render(null);
        }
        return compact('testDt');
    }
    
    public function store(StoreRequest $request)
    {
        $input = request()->all();
        $input['created_by'] = auth()->user()->id ?? null;
        $this->testModel->create($input);
        return response()->json(['type' => 'success', 'title' => 'Created!', 'message' => 'Successfully Created']);
    }

    public function update(UpdateRequest $request, $id)
    {
        $test = $this->testModel->findOrFail($id);
        $input = request()->all();
        $input['updated_by'] = auth()->user()->id ?? null;
        $test->updateOrCreate(['id' => $id], $input);
        return response()->json(['type' => 'success', 'title' => 'Updated!', 'message' => 'Successfully Updated']);
    }

    /**
     * Remove the specified resource from storage.
     * @param type $id
     * @return response
     */
    public function destroy($id)
    {
        $test = $this->testModel->findOrFail($id);
        $test->delete();
        return response()->json(['type' => 'success', 'title' => 'Deleted!', 'message' => 'Successfully Deleted']);
    }
    
}
