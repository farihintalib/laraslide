<?php

/**
 * Generate at 2021-03-28 02:40:36. Clear comment if this file being used
 */

namespace App\Http\Controllers;

use App\DataTables\Attendance\AttendanceDt;
use App\Http\Requests\Attendance\StoreRequest;
use App\Http\Requests\Attendance\UpdateRequest;
use App\Models\Attendance;

class AttendanceController extends Controller
{
    
    protected $attendanceModel;
    
    public function __construct() {
        $this->attendanceModel = new Attendance();
    }
    
    public function index()
    {
        $title = __('attendance');
        if (!empty(request()->get('table'))) {
            return $this->dataTable();
        }
        $compact = array_merge($this->dataTable(), compact('title'));
        return view('attendance.index', $compact);
    }

    public function dataTable() {
        $attendanceDt = new AttendanceDt();
        if (request()->get('table') == 'attendanceDt') {
            return $attendanceDt->render(null);
        }
        return compact('attendanceDt');
    }
    
    public function store(StoreRequest $request)
    {
        $input = request()->all();
        $input['created_by'] = auth()->user()->id ?? null;
        $this->attendanceModel->create($input);
        return response()->json(['type' => 'success', 'title' => 'Created!', 'message' => 'Successfully Created']);
    }

    public function update(UpdateRequest $request, $id)
    {
        $attendance = $this->attendanceModel->findOrFail($id);
        $input = request()->all();
        $input['updated_by'] = auth()->user()->id ?? null;
        $attendance->updateOrCreate(['id' => $id], $input);
        return response()->json(['type' => 'success', 'title' => 'Updated!', 'message' => 'Successfully Updated']);
    }

    /**
     * Remove the specified resource from storage.
     * @param type $id
     * @return response
     */
    public function destroy($id)
    {
        $attendance = $this->attendanceModel->findOrFail($id);
        $attendance->delete();
        return response()->json(['type' => 'success', 'title' => 'Deleted!', 'message' => 'Successfully Deleted']);
    }
    
}
