<?php

/**
 * Generate at 2021-03-28 02:41:06. Clear comment if this file being used
 */

namespace App\Http\Requests\Employee;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validator = [
            'first_name' => 'required',
            'email' => 'email',
        ];
        return $validator;
    }
    
    /**
     * Remove if not use Json Request
     */
    public function failedValidation(Validator $validator) {
        if ($validator->fails()) {
            throw new HttpResponseException(
                    response()->json(['errors' => $validator->messages()], 422)
            );
        }
    }
}
