<?php

/**
 * Generate at 2021-03-26 17:11:12. Clear comment if this file being used
 */

namespace App\Http\Requests\Test;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
    
    /**
     * Remove if not use Json Request
     */
    public function failedValidation(Validator $validator) {
        if ($validator->fails()) {
            throw new HttpResponseException(
                    response()->json(['errors' => $validator->messages()], 422)
            );
        }
    }
}
