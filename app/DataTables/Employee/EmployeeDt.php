<?php

/**
 * Generate at 2021-03-28 02:41:06. Clear comment if this file being used
 */

namespace App\DataTables\Employee;

use App\Models\Employee;
use App\Services\BaseService;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class EmployeeDt extends DataTable {

    protected $tableId;
    protected $baseService;
    protected $employeeModel;

    public function __construct() {
        $this->tableId = 'employeeDt';
        $this->baseService = new BaseService();
        $this->employeeModel = new Employee();
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables()
                        ->eloquent($query)
                        ->addIndexColumn()
                        ->addColumn('action', function($query) {
                            $id = $query->getKey();
                            $data = [
                                'edit' => [
                                    'swalId' => 'employeeDtUpdate',
                                    'id' => $id,
                                    'first_name' => $query->first_name,
                                    'last_name' => $query->last_name,
                                    'email' => $query->email,
                                    'company_id' => $query->company_id,
                                    'created_by' => $query->created_by,
                                    'updated_by' => $query->updated_by,
                                    'created_at' => $query->created_at, 'updated_at' => $query->updated_at,
                                ],
                                'delete' => [
                                    'method' => 'delete',
                                    'title' => __('are you sure'),
                                    'message' => __('you wont be able to revert this'),
                                    'route' => route('employees.destroy', [$id]),
                                ],
                            ];
                            return $this->baseService->yajraService->actionView($query, $data);
                        })
                        ->with('recordssaya', 'saya')
                        ->only(array_column($this->getColumns(), 'data'))
                        ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \DummyModel $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        $query = $this->employeeModel
                ->with(['company'])
                ->when(request()->first_name, function($query) {
                    $keyword = request()->first_name;
                    $query->where('first_name', 'LIKE', "%{$keyword}%");
                })
                ->when(request()->last_name, function($query) {
                    $keyword = request()->last_name;
                    $query->where('last_name', 'LIKE', "%{$keyword}%");
                })
                ->when(request()->email, function($query) {
            $keyword = request()->email;
            $query->where('email', 'LIKE', "%{$keyword}%");
        });
        return $query;
//        dd($query->get());
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        $data = [
            'table' => '"' . $this->tableId . '"',
            'first_name' => '$("#first_name").val()',
            'last_name' => '$("#last_name").val()',
            'email' => '$("#email").val()',
        ];
        $appendData = $this->baseService->yajraService->makeDataScript($data);
        return $this->builder()
                        ->setTableId($this->tableId)
                        ->columns($this->getColumns())
                        ->minifiedAjax('', $appendData)
                        ->dom("<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-4'f><'col-sm-12 col-md-4 dataTables_buttons'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>")
                        ->parameters([
                            'buttons' => [
                                'pdf', 'excel',
                                $this->baseService->yajraService->btnAdd(['swalId' => 'employeeDtStore']),
                            ],
                            'order' => [
                                1, 'asc'
                            ],
                            'responsive' => true,
                            'searching' => true,
                            'autoWidth' => false,
                            'drawCallback' => 'function(settings){
                                console.log(settings.json);
                                $("#result").html(settings.json.recordsTotal);
                            }',
        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        return [
            Column::computed('DT_RowIndex', '#')->exportable(false)->printable(false)->width(20)->addClass('text-center'),
            Column::make('first_name'),
            Column::make('last_name')->orderable(false)->searchable(false),
            Column::make('email'),
            Column::make('company.name')->title('Company'),
            Column::computed('action')->exportable(false)->printable(false)->width(80)->addClass('text-center all'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'Employee_' . date('YmdHis');
    }

}
