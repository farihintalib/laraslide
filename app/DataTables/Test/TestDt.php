<?php

/**
 * Generate at 2021-03-26 17:11:12. Clear comment if this file being used
 */

namespace App\DataTables\Test;

use App\Models\Test;
use App\Services\BaseService;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class TestDt extends DataTable
{
    protected $tableId;
    protected $baseService;
    protected $testModel;
    
    public function __construct() {
        $this->tableId = 'testDt';
        $this->baseService = new BaseService();
        $this->testModel = new Test();
    }
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('action', function($query){
                $id = $query->getKey();
                $data = [
                    'edit' => [
                        'swalId' => 'testDtUpdate',
                        'id' => $id,
                        'name' => $query->name,'description' => $query->description,
                    ],
                    'delete' => [
                        'method' => 'delete',
                        'title' => __('are you sure'),
                        'message' => __('you wont be able to revert this'),
                        'route' => route('tests.destroy',[$id]),
                    ],
                ];
                return $this->baseService->yajraService->actionView($query, $data);
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \DummyModel $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return $this->testModel->query();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $data = [
            'table' => '"' . $this->tableId . '"',
        ];
        $appendData = $this->baseService->yajraService->makeDataScript($data);
        return $this->builder()
                    ->setTableId($this->tableId)
                    ->columns($this->getColumns())
                    ->minifiedAjax('', $appendData)
                    ->dom("<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-4'f><'col-sm-12 col-md-4 dataTables_buttons'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>")
                    ->parameters([
                        'buttons' => [
                            $this->baseService->yajraService->btnAdd(['swalId' => 'testDtStore']),
                        ],
                        'order' => [
                            1, 'asc'
                        ],
                        'responsive' => true,
                        'searching' => true,
                        'autoWidth' => false,
        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex', '#')->exportable(false)->printable(false)->width(20)->addClass('text-center'),
            Column::make('name'),Column::make('description'),
            Column::computed('action')->exportable(false)->printable(false)->width(80)->addClass('text-center all'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Test_' . date('YmdHis');
    }
}
