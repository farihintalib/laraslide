<?php

namespace App\Services;

use Illuminate\Support\HtmlString;

class YajraService {

    /**
     * Check checkbox
     * See sample App\DataTables\EniscopeAlert\EniscopeChannelsDataTable;
     * @param type $checkboxId
     * @param type $value
     * @return string checked or null;
     */
    public function checkboxCheck($checkboxId, $value) {
        if (isset(request()->$checkboxId) && in_array($value, request()->$checkboxId)) {
            $check = 'checked';
        } else {
            $check = null;
        }
        return $check;
    }

    /**
     * Split checkbox value
     * See sample App\DataTables\EniscopeAlert\EniscopeChannelsDataTable;
     * @param type $checkboxId
     * @return type
     */
    public function splitCheckboxValue($checkboxId) {
        return '$.grep($("#' . $checkboxId . '").val().split(","), function (item, index) {
                    return index == $.inArray(item, $("#' . $checkboxId . '").val().split(","));
                });';
    }

    /**
     * Get view for input checkbox in column action
     * See sample App\DataTables\EniscopeAlert\EniscopeChannelsDataTable;
     * @param type $checkboxId
     * @param type $check
     * @param type $passDataTo
     * @param type $checkboxValue
     * @return type
     */
    public function checkboxSingleView(array $params) {
        $check = $this->checkboxCheck($params['checkboxId'], $params['checkboxValue']);
        $data = [
            'cbx' => [
                'checkboxId' => $params['checkboxId'],
                'passDataTo' => $params['passDataTo'] ?? null,
                'checkboxValue' => $params['checkboxValue'] ?? null,
                'checkboxName' => $params['checkboxName'] ?? null,
                'checkboxFn' => $params['checkboxFn'] ?? null,
            ],
        ];
        return view('services.yajra-service.checkbox_single', compact('check', 'data'))->render();
    }

    /**
     * Get view for input checkbox in header action
     * See sample App\DataTables\EniscopeAlert\EniscopeChannelsDataTable;
     * @param type $checkboxId
     * @param type $passDataTo
     * @return type
     */
    public function checkboxAllView(array $params) {
        $data = [
            'cbx' => [
                'checkboxId' => $params['checkboxId'],
                'passDataTo' => $params['passDataTo'],
                'label' => $params['label'],
            ],
        ];
        return view('services.yajra-service.checkbox_all', compact('data'))->render();
    }

    public function actionView($query, $data) {
        return view('services.yajra-service.action', compact('query', 'data'))->render();
    }

    public function btnAdd(array $data) {
        return [
            'extend' => 'add',
            'className' => 'btn-primary',
            'action' => 'function ( e, dt, node, config ) { upsert({"swalId":"' . $data['swalId'] . '"}) }',
        ];
    }

    public function btnMore(array $params) {
        $data = [
            'delete' => $params['delete']
        ];
        return [
            'extend' => 'custom',
            'className' => 'buttons-more btn-info',
            'text' => view('services.yajra-service.dropdown_more', compact('data'))->render(),
            'attr' => [
                'id' => $params['tableId'] . 'More',
                'style' => 'display:none',
                'data-toggle' => 'dropdown',
            ],
        ];
    }

    public function makeDataScript(array $data) {
        $script = '';
        foreach ($data as $key => $value) {
            $script .= PHP_EOL . "data.{$key} = {$value};";
        }
        return $script;
    }

    public function script($datatable) {
        return new HtmlString(
                sprintf('$("#%1$s").DataTable(%2$s)', $datatable->html()->getTableAttribute('id'), $datatable->html()->generateJson())
        );
    }

}
