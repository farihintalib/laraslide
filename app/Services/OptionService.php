<?php

namespace App\Services;

use Illuminate\Support\Arr;

class OptionService {

    public function __construct() {
        
    }

    /**
     * Get only selected option based on key
     * @param type $key
     * @param type $option
     * @return array
     */
    public function get($key, $option) {
        if (method_exists($this, $option)) {
            $options = $this->$option(['selected' => $key]);
            $selected = array_filter($options, function($value) {
                return $value["data-selected"] == 'selected';
            });
            return Arr::first($selected);
        } else {
            return [];
        }
    }

    public function activityType(array $data = []) {
        return [
            'internal' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'internal'),
                'data-color' => 'success',
                'data-value' => __('Internal'),
                'data-key' => 'internal',
            ],
            'public' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'public'),
                'data-color' => 'danger',
                'data-value' => __('Public'),
                'data-key' => 'public',
            ],
        ];
    }

    public function activityIsForm(array $data = []) {
        return [
            'no' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'no'),
                'data-color' => 'danger',
                'data-value' => __('No'),
                'data-key' => 'no',
            ],
            'yes' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'yes'),
                'data-color' => 'success',
                'data-value' => __('Yes'),
                'data-key' => 'yes',
            ],
        ];
    }

    public function activityStatus(array $data = []) {
        return [
            'draft' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'draft'),
                'data-color' => 'info',
                'data-value' => __('Draft'),
                'data-key' => 'draft',
            ],
            'pending' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'pending'),
                'data-color' => 'secondary',
                'data-value' => __('Pending'),
                'data-key' => 'pending',
            ],
            'revert' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'revert'),
                'data-color' => 'warning',
                'data-value' => __('Revert'),
                'data-key' => 'revert',
            ],
            'approve' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'approve'),
                'data-color' => 'success',
                'data-value' => __('Approve'),
                'data-key' => 'approve',
            ],
            'reject' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'reject'),
                'data-color' => 'danger',
                'data-value' => __('Reject'),
                'data-key' => 'reject',
            ],
        ];
    }

    public function activityApprovalStatus(array $data = []) {
        return [
            'pending' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'pending'),
                'data-color' => 'secondary',
                'data-value' => __('Pending'),
                'data-key' => 'pending',
            ],
            'revert' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'revert'),
                'data-color' => 'warning',
                'data-value' => __('Revert'),
                'data-key' => 'revert',
            ],
            'approve' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'approve'),
                'data-color' => 'success',
                'data-value' => __('Approve'),
                'data-key' => 'approve',
            ],
            'reject' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'reject'),
                'data-color' => 'danger',
                'data-value' => __('Reject'),
                'data-key' => 'reject',
            ],
        ];
    }

    public function userActive(array $data = []) {
        return [
            'active' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'active'),
                'data-color' => 'success',
                'data-value' => __('Active'),
                'data-key' => 'active',
            ],
            'inactive' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'inactive'),
                'data-color' => 'danger',
                'data-value' => __('Inactive'),
                'data-key' => 'inactive',
            ],
        ];
    }

    public function organisationActive(array $data = []) {
        return [
            'active' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'active'),
                'data-color' => 'success',
                'data-value' => __('Active'),
                'data-key' => 'active',
            ],
            'inactive' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'inactive'),
                'data-color' => 'danger',
                'data-value' => __('Inactive'),
                'data-key' => 'inactive',
            ],
        ];
    }

    public function organisationUserStatus(array $data = []) {
        return [
            'verify' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'verify'),
                'data-color' => 'warning',
                'data-value' => __('Verification'),
                'data-key' => 'verify',
            ],
            'active' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'active'),
                'data-color' => 'success',
                'data-value' => __('Active'),
                'data-key' => 'active',
            ],
            'inactive' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'inactive'),
                'data-color' => 'danger',
                'data-value' => __('Inactive'),
                'data-key' => 'inactive',
            ],
        ];
    }

    public function userStatus(array $data = []) {
        return [
            'verify' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'verify'),
                'data-color' => 'warning',
                'data-value' => __('Verification'),
                'data-key' => 'verify',
            ],
            'active' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'active'),
                'data-color' => 'success',
                'data-value' => __('Active'),
                'data-key' => 'active',
            ],
            'inactive' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'inactive'),
                'data-color' => 'danger',
                'data-value' => __('Inactive'),
                'data-key' => 'inactive',
            ],
        ];
    }

    public function permissionActive(array $data = []) {
        return [
            'active' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'active'),
                'data-color' => 'success',
                'data-value' => __('Active'),
                'data-key' => 'active',
            ],
            'inactive' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'inactive'),
                'data-color' => 'danger',
                'data-value' => __('Inactive'),
                'data-key' => 'inactive',
            ],
        ];
    }

    public function roleActive(array $data = []) {
        return [
            'active' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'active'),
                'data-color' => 'success',
                'data-value' => __('Active'),
                'data-key' => 'active',
            ],
            'inactive' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'inactive'),
                'data-color' => 'danger',
                'data-value' => __('Inactive'),
                'data-key' => 'inactive',
            ],
        ];
    }

    public function yesNo(array $data = []) {
        return [
            'yes' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'yes'),
                'data-color' => 'success',
                'data-value' => __('Yes'),
                'data-key' => 'yes',
            ],
            'no' => [
                'data-selected' => $this->selected($data['selected'] ?? '', 'no'),
                'data-color' => 'danger',
                'data-value' => __('No'),
                'data-key' => 'no',
            ],
        ];
    }

    public function selected($select, $key) {
        return $select == $key ? 'selected' : '';
    }

}
