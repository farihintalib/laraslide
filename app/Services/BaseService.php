<?php

namespace App\Services;

use App\Services\OptionService;
use App\Services\YajraService;
use App\Traits\GeneralTrait;
use App\Traits\ResponseTrait;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Validator;

class BaseService {

//    use GeneralTrait;
//    use ResponseTrait;

    public $optionService;
    public $yajraService;

    public function __construct() {
        $this->optionService = new OptionService();
        $this->yajraService = new YajraService();
    }

    public function validateRequest($request) {
        $validator = Validator::make(request()->all(), $request->rules());
        $request->failedValidation($validator);
    }

    public function option($options) {
        $select = [[]];
        foreach ($options as $key => $value) {
            array_push($select, ['id' => $key, 'text' => $value]);
        }
        return response()->json(['results' => $select]);
    }

    public function authorizeRequest($authorize) {
        if (!$authorize) {
            throw new AuthorizationException;
        }
        return;
    }

}
