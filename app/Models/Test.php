<?php

/**
 * Generate at 2021-03-26 17:11:12. Clear comment if this file being used
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    public $incrementing = true;
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $primaryOrCompositeKey = ['id'];
    protected $table = 'tests';
    protected $fillable = [
        'name','description'
    ];
    
    /* =========================================================================
     * Relationship
     * =========================================================================
     */
    
    /* =========================================================================
     * Mutator
     * =========================================================================
     */
}
