<?php

/**
 * Generate at 2021-03-28 02:40:36. Clear comment if this file being used
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    public $incrementing = true;
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $primaryOrCompositeKey = ['id'];
    protected $table = 'attendances';
    protected $fillable = [
        'name','description'
    ];
    
    /* =========================================================================
     * Relationship
     * =========================================================================
     */
    
    /* =========================================================================
     * Mutator
     * =========================================================================
     */
}
