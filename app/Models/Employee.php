<?php

/**
 * Generate at 2021-03-28 02:41:06. Clear comment if this file being used
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public $incrementing = true;
    public $timestamps = true;
    protected $primaryKey = 'id';
    protected $primaryOrCompositeKey = ['id'];
    protected $table = 'employees';
    protected $fillable = [
        'first_name','last_name','email','company_id','created_by','updated_by','created_at','updated_at'
    ];
    
    /* =========================================================================
     * Relationship
     * =========================================================================
     */
    
    public function company(){
        return $this->belongsTo(Company::class);
    }
    
    /* =========================================================================
     * Mutator
     * =========================================================================
     */
}
