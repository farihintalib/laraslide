<?php

/**
 * Generate at 2021-03-28 02:40:12. Clear comment if this file being used
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public $incrementing = true;
    public $timestamps = true;
    protected $primaryKey = 'id';
    protected $primaryOrCompositeKey = ['id'];
    protected $table = 'companies';
    protected $fillable = [
        'name','email','created_by','updated_by','created_at','updated_at'
    ];
    
    /* =========================================================================
     * Relationship
     * =========================================================================
     */
    
    /* =========================================================================
     * Mutator
     * =========================================================================
     */
}
