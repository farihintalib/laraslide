<?php

use App\Http\Controllers\AttendanceController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\GreetingController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\TutorialController;
use App\Http\Controllers\UserController;
use App\Http\Controllers;
use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Auth::routes();
Route::get('/', function () {
    return view('welcome');
});
Route::get('tutorials', [TutorialController::class, 'index'])->name('tutorials.index');

Route::resource('tests', TestController::class)->except([
    'create', 'show', 'edit'
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('attendances', AttendanceController::class)->except([
    'create', 'show', 'edit'
]);

Route::get('/hello/{name}', function ($name) {
    return "Hello {$name}";
//    return 'Hello ' . $name;
})->name('hello_name');

Route::get('greeting', [GreetingController::class, 'index'])->name('greeting.index');
Route::get('greeting/{name}', [GreetingController::class, 'show'])->name('greeting.show');

Route::get('page/1', function () {
    return view('page.1');
})->name('page.1');
Route::get('page/2', function () {
//    App::setLocale(request()->lang);
    return view('page.2');
})->name('page.2');
Route::get('page/3', function () {
    return view('page.3');
})->name('page.3');

Route::get('set_locale/{locale}', function ($locale) {
    session()->put('locale', $locale);
    return 'Set language ' . $locale;
})->name('set_locale');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/profile', function () {
        return view('profile');
    })->name('profile');
});

Route::group(['middleware' => ['auth']], function () {
    Route::resource('users', UserController::class);
});

Route::group(['middleware' => ['auth']], function () {
    Route::resource('employees', EmployeeController::class)->except([
        'create', 'show', 'edit'
    ]);
    Route::resource('companies', CompanyController::class)->except([
        'create', 'show', 'edit'
    ]);
});

Route::get('charts', [Controllers\ChartController::class,'index'])->name('charts.index');
