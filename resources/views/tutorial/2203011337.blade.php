@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>CLI</p>
    </div>
    @markdown @verbatim
    php artisan make:request Attendance\UpdateRequest @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>UpdateRequest</p>
    </div>
    @markdown @verbatim
    namespace App\Http\Requests\Attendance;

    use Illuminate\Foundation\Http\FormRequest;

    class UpdateRequest extends FormRequest {

        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize() {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules() {
            return [
                'id' => 'required|integer',
                'name' => 'required|max:10',
                'date' => 'required|date',
                'att_in' => 'required',
                'att_out' => 'required',
            ];
        }

    } @endverbatim @endmarkdown
     
    <div class="callout callout-success">
        <p>Controller</p>
    </div>
    
    @markdown @verbatim
    use App\Http\Requests\Attendance\UpdateRequest;
    
    public function update(UpdateRequest $request, $id) { ... } @endverbatim @endmarkdown
     
@endslot @slot('timeline_footer')
@endslot @endcomponent