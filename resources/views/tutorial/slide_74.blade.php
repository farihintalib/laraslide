@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code inside UserController@destroy</p>
    </div>
    @markdown @verbatim
    public function destroy(User $user) {
        $user->delete();
        return redirect(back()->getTargetUrl());
    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent