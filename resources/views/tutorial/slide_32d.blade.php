@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>You are given the function in GreetingController@download. Write your route.</p>
    </div>
    @markdown @verbatim
    protected function download() {
        $pathToFile = 'C:\laragon\www\laraslide\public\images\tutorial\08.pdf';
        return response()->file($pathToFile);
    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://laravel.com/docs/9.x/responses">Full Documentation</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent