@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code inside Route</p>
    </div>
    @markdown @verbatim
    Route::resource('photos', 'PhotoController');
    Route::resource('albums', 'AlbumController'); @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code using CLI</p>
    </div>
    @markdown @verbatim
    php artisan make:controller PhotoController --resource
    php artisan make:controller AlbumController --resource --model=Album @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside controller</p>
    </div>
    @markdown @verbatim
    dd(\Route::current(), \Route::currentRouteName(), \Route::currentRouteAction()); @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent