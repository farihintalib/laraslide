@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code inside 'layouts\app.blade.php'</p>
    </div>
    @markdown @verbatim
    <html lang="{{ app()->getLocale() }}"> @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside lang 'en\page.php'</p>
    </div>
    @markdown @verbatim
    return [
        'title_2' => 'Title Page 2',
        'body_2' => 'Body Page 2',
        'footer_2' => 'Footer Page 2',
    ]; @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside lang 'ms\page.php'</p>
    </div>
    @markdown @verbatim
    return [
        'title_2' => 'Tajuk Mukasurat 2',
        'body_2' => 'Kandungan Mukasurat 2',
        'footer_2' => 'Bawah Mukasurat 2',
    ]; @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'page\2.blade.php'</p>
    </div>
    @markdown @verbatim
    @section('main-content')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{__('page.title_2')}}</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                {{__('page.body_2')}}
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                {{__('page.footer_2')}}
            </div>
            <!-- /.box-footer-->
        </div>
    @endsection @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent