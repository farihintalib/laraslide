@component('layouts.components.timeline_item',['i'=>$i,'color'=>'bg-aqua']) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code at \route folder</p>
    </div>
    @markdown @verbatim
    Route::get('/hello_world', function () {
        return 'Hello World';
    })->name('hello_world'); @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Change GET to another method POST, PUT, DELETE</p>
    </div>
@endslot @slot('timeline_footer')
@endslot @endcomponent