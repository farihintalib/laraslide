@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>CLI</p>
    </div>
    @markdown @verbatim
    php artisan make:import AttendancesImport --model=Attendance @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Import</p>
    </div>
    
    @markdown @verbatim
    namespace App\Imports;
    
    use App\Models\Attendance;
    use Maatwebsite\Excel\Concerns\ToModel;
    use Maatwebsite\Excel\Concerns\WithHeadingRow;

    class AttendancesImport implements ToModel, WithHeadingRow {

        /**
         * @param array $row
         *
         * @return \Illuminate\Database\Eloquent\Model|null
         */
        public function model(array $row) {
            return new Attendance([
                'name' => $row['name'],
                'date' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['date'])->format('Y-m-d'),
                'att_in' => $row['att_in'],
                'att_out' => $row['att_out'],
                'status' => $row['status'],
            ]);
        }

        public function headingRow(): int {
            return 1;
        }

    } @endverbatim @endmarkdown
     
    <div class="callout callout-success">
        <p>Controller</p>
    </div>
    
    @markdown @verbatim
    use App\Imports\AttendancesImport;
    
    public function storeExcel(Request $request) {
        if ($request->hasFile('excel')) {
            Excel::import(new AttendancesImport, $request->file('excel'));
        }
        return redirect()->back();
    } @endverbatim @endmarkdown
     
@endslot @slot('timeline_footer')
@endslot @endcomponent