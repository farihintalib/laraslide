@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <img class="img-responsive" src="{{url('/images/tutorial/picture23.jpg')}}">
@endslot @slot('timeline_footer')
@endslot @endcomponent