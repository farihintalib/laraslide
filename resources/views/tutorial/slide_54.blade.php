@component('layouts.components.timeline_item',['color'=>'bg-aqua']) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <object class="img-responsive" type="image/svg+xml" data="{{url('/images/tutorial/picture30.svg')}}"></object>
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://laravel.com/docs/9.x/localization">Full Documentation</a></li>
                    <li><a href="https://www.w3schools.com/tags/ref_language_codes.asp">ISO Code</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent