@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Default laravel uses the utf8mb4 charset which uses more space than the utf8 charset. Change the mysql settings to allow for bigger keys or just change the charset to utf8 and the collation to utf8_general_ci in config/database.php</p>
    </div>
    <div class="callout callout-success">
        <p>Configure database and create db laravel. Write this code inside command</p>
    </div>
    @markdown @verbatim
    php artisan migrate @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent