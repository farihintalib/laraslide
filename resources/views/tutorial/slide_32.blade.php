@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Copy 'welcome.blade.php' to 'views/greeting' and rename to 'show.blade.php'. Change the text to variable</p>
    </div>
    @markdown @verbatim
    Documentation

    {{$greeting}} @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside GreetingController@show</p>
    </div>
    @markdown @verbatim
    public function show($name) {
        $greeting = $this->greeting . $name;
        // return view('greeting.show',['greeting'=>$greeting]);
        // return view('greeting.show')->with('greeting',$greeting);
        return view('greeting.show',compact('greeting'));
    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent