@component('layouts.components.timeline_item',['i'=>$i,'color'=>'bg-aqua']) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Install yajra/laravel-datatables via composer. Write this code inside command</p>
    </div>
    @markdown @verbatim
    composer require yajra/laravel-datatables @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Route</p>
    </div>
    @markdown @verbatim
    Route::get('users-yajra', 'UserController@indexYajra')->name('users.index-yajra'); @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Controller</p>
    </div>
    @markdown @verbatim
    public function indexYajra(UsersDataTable $usersDt) {
        if (request()->get('table') == 'usersDt') {
            return $usersDt->render(null);
        }
        return view('user.index_yajra',compact('usersDt'));
    } @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Create new file 'app\DataTables\User\UsersDataTable.php' with following <a href="{{url('d?file=app\DataTables\User\UsersDataTable.php')}}">code</a></p>
    </div>
    <div class="callout callout-success">
        <p>Create new file 'resources\views\user\index_yajra.blade.php' with following <a href="{{url('d?file=resources\views\user\index_yajra.blade.php')}}">code</a></p>
    </div>
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://yajrabox.com/docs/laravel-datatables/master">Yajra Datatables</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent