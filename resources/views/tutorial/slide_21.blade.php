@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code to any Route function</p>
    </div>
    @markdown @verbatim
    Route::group(['middleware' => ['auth']], function () {
        Route::get('/hello_world', function () {
            return 'Hello World';
        })->name('hello_world');
    });

    Route::get('/login', function () {
        return 'This is login';
    })->name('login'); @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent