@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Scaffold new migration to add 'department_id' to 'users' table. Write this code inside command</p>
    </div>
    @markdown @verbatim
    php artisan make:migration add_department_id_to_users_table --table=users @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside '/database/migration/..._add_department_id_to_users_table'</p>
    </div>
    @markdown @verbatim
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('department_id')->unsigned()->nullable();
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['department_id']);
            $table->dropColumn('department_id');
        });
    } @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside command</p>
    </div>
    @markdown @verbatim
    php artisan migrate @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'User' model</p>
    </div>
    @markdown @verbatim
    protected $fillable = [
        'name', 'email', 'password', 'department_id',
    ];
    
    public function departmentIdDepartment() {
        return $this->belongsTo('App\Department', 'department_id');
    } @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'user/edit.blade.php'</p>
    </div>
    @markdown @verbatim
    <form method="POST" action="{{ route('users.update',['user'=>$user->id]) }}">
        @csrf
        @method('PUT')
        <strong>Name:</strong> <input type="text" name="name" value="{{$user->name}}" required maxlength="254" placeholder="First name"><br/>
        <strong>Email:</strong> <input type="email" name="email" value="{{$user->email}}" required placeholder="Email"><br/>
        <strong>Department:</strong> <select name="department_id"><option>Select Department</option>
                                        @foreach (\App\Department::pluck('name', 'id') as $key => $value)
                                          <option value="{{ $key }}" {{ ($key == $user->department_id) ? 'selected' : '' }}> 
                                              {{ $value }} 
                                          </option>
                                        @endforeach </select><br/>
        <button type="submit">Update</button>
    </form> @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'user/show.blade.php'</p>
    </div>
    @markdown @verbatim
    <strong>Department:</strong> {{$user->departmentIdDepartment->name ?? null}} @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent