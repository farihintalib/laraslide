@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>CLI</p>
    </div>
    @markdown @verbatim
    php artisan make:migration add_soft_deletes_to_attendances_table @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Schema</p>
    </div>
    
    @markdown @verbatim
    public function up() {
        Schema::table('attendances', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    public function down() {
        Schema::table('attendances', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    } @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>CLI</p>
    </div>
    @markdown @verbatim
    php artisan migrate @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Model</p>
    </div>
    @markdown @verbatim
    use Illuminate\Database\Eloquent\SoftDeletes; 
    
    use HasFactory, SoftDeletes; @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://laravel.com/docs/9.x/migrations">Migration</a></li>
                    <li><a href="https://laravel.com/docs/9.x/eloquent#soft-deleting">Soft Delete</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent