@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>CLI</p>
    </div>
    @markdown @verbatim
    php artisan make:seeder AttendanceLookupSeeder @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Seeder</p>
    </div>
    @markdown @verbatim
    use App\Models\AttendanceLookup;
    
    public function run() {
        AttendanceLookup::firstOrCreate(['name' => 'late'], ['label' => 'Late']);
        AttendanceLookup::firstOrCreate(['name' => 'present'], ['label' => 'Present']);
    } @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>CLI</p>
    </div>
    @markdown @verbatim
    php artisan db:seed --class=AttendanceLookupSeeder @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>DatabaseSeeder</p>
    </div>
    @markdown @verbatim
    public function run() {
        // \App\Models\User::factory(10)->create();
        $this->call([
            AttendanceLookupSeeder::class,
        ]);
    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent