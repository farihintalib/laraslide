@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code inside UserController@store</p>
    </div>
    @markdown @verbatim
    public function store(Request $request) {
        $input = $request->all();
        $user = new User();
        $user->fill($input);
        $user->password = \Hash::make('12345678');
        $user->save();
        return redirect(route('users.index'));
    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent