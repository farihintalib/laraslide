@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code to any Route function</p>
    </div>
    @markdown @verbatim
    dd(\Route::current(), \Route::currentRouteName(), \Route::currentRouteAction()); @endverbatim @endmarkdown
    @markdown @verbatim
    dd(request()->route(), request()->route()->getName()); @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent