@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Modified the code</p>
    </div>
    @markdown @verbatim
    public function index() {
        return "{$this->greeting()} World";
    }

    public function show($name) {
        return "{$this->greeting()} {$name}";
    }

    protected function greeting() {
        return 'Hello';
    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')

@endslot @endcomponent