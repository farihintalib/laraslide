@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>CLI</p>
    </div>
    @markdown @verbatim
    php artisan make:migration create_attendance_lookups_table
    php artisan make:migration add_status_id_to_attendances_table @endverbatim @endmarkdown
    
    
    <div class="callout callout-success">
        <p>Schema (create_attendance_lookups_table)</p>
    </div>
    
    @markdown @verbatim
    public function up() {
        Schema::create('attendance_lookups', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('label');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('attendance_lookups');
    } @endverbatim @endmarkdown
     
    <div class="callout callout-success">
        <p>Schema (add_status_id_to_attendances_table)</p>
    </div>
    
    @markdown @verbatim
    public function up() {
        Schema::table('attendances', function (Blueprint $table) {
            $table->unsignedBigInteger('status_id')->nullable();
            $table->foreign('status_id')->references('id')->on('attendance_lookups')->onDelete('set Null')->onUpdate('cascade');
        });
    }

    public function down() {
        Schema::table('attendances', function (Blueprint $table) {
            $table->dropForeign(['status_id']);
            $table->dropColumn('status_id');
        });
    } @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>CLI</p>
    </div>
    @markdown @verbatim
    php artisan migrate 
    php artisan make:model AttendanceLookup @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Model (Attendance)</p>
    </div>
    @markdown @verbatim
    public function attendanceLookupByStatusId() {
        return $this->belongsTo(AttendanceLookup::class, 'status_id');
    } @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Controller (AttendanceController)</p>
    </div>
    @markdown @verbatim
    public function index(Request $request) {
        $attendances = Attendance::with(['attendanceLookupByStatusId'])->get();
        return view('attendance.index', compact('attendances'));
    } @endverbatim @endmarkdown
    
    
    <div class="callout callout-success">
        <p>View</p>
    </div>
    @markdown @verbatim
    <th>Status</th>
    
    <td>{{$attendance->attendanceLookupByStatusId->label ?? null}}</td> @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent