@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Create new component 'layouts.components.table_ajax.blade.php'. Write this code inside 'table_ajax.blade.php'</p>
    </div>
    @markdown @verbatim
    <table id="{{ $tname }}" class="table display stripe table-hover" width="100%">
        <thead>
            <tr class='bg-green'>
                {{ $thead }}
            </tr>
        </thead>
    </table>
    @push('scriptsDocumentReady')
        var {{ $tname }} = $('#{{ $tname }}').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            @if(isset($firstScript))
                {{ $firstScript }}
            @endif
            ajax:{
                "url": "{{ $url }}",
                "dataType": "json",
                "type": "GET",
                "timeout": 30000,
            },
            columns: [
                {{ $tbody }}
            ],
        });
        @if(!isset($no_number))
            {{ $tname }}.on('order.dt search.dt', function () {
                {{ $tname }}.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, {{ $tname }}) {
                    cell.innerHTML = {{ $tname }} + 1;
                });
            }).draw();
        @endif
        @if(isset($secondScript))
            {{ $secondScript }}
        @endif
    @endpush @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'layouts/scripts.blade.php'</p>
    </div>
    @markdown @verbatim
    <script>
        $(document).ready(function () {
            @stack('scriptsDocumentReady')
        });
    </script> @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'user/index.blade.php' replace raw table with component</p>
    </div>
    @markdown @verbatim
    @component('layouts.components.table_ajax', ['tname' => 'users_table_ajax']) @slot('url')
        {{ route('users.index')}}
    @endslot @slot('thead')
        <th style='width: 30px;'>{{__('general.number')}}</th>
        <th>Name</th>
        <th>Email</th>
        <th style="width:66px;">{{__('general.action')}}</th>
    @endslot @slot('tbody')
        { data: 'DT_RowIndex', orderable: false, searchable: false },
        { data: 'name', name: 'name' },
        { data: 'email', name: 'email' },
        { data: 'action', name: 'action' },
    @endslot @slot('firstScript')
    @endslot @slot('secondScript')
    @endslot @endcomponent @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Write this code inside UserController</p>
    </div>
    @markdown @verbatim
    public function index() {
        if (Request()->ajax()) {
            $modelDatatables = $this->ajax(Request(), 'index');
            return $modelDatatables->make(true);
        }
        return view('user.index');
    }
    
    public function ajax($request, $method) {
        if ($method == 'index') {
            $modelDatatables = Datatables::of(User::query())
                    ->addIndexColumn()
                    ->addColumn('action', function ($query) {
                        $btnView = "<a href='".route('users.show',['user'=>$query->id])."'>View</a>";
                        $btnEdit = "<a href='".route('users.edit',['user'=>$query->id])."'>Edit</a>";
                        $btnDelete = "";
                        return $btnView.' '.$btnEdit.' '.$btnDelete;
                    });
        }
        return $modelDatatables;
    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent