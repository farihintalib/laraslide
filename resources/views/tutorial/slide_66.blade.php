@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Do Register and Login</p>
    </div>
    <div class="callout callout-success">
        <p>Fix for button Sign Out</p>
    </div>
    @markdown @verbatim
    <a href="{{ route('logout') }}" class="btn btn-default btn-flat" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();$('#loading').show();">Sign out</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form> @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Fix for route profile</p>
    </div>
    @markdown @verbatim
    <a href="{{url('profile')}}" class="btn btn-default btn-flat">Profile</a> @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Fix for current user name</p>
    </div>
    @markdown @verbatim
    {{Auth::check() ? ucfirst(Auth::user()->name) : 'Guest'}}
    @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent