@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code inside AttendanceController@index</p>
    </div>
    @markdown @verbatim
    $attendances = collect([
        ['id' => 1, 'name' => 'Muhamad Fadhil','date'=>'2019-11-01', 'att_in' => '8:00','att_out' => '5:00'],
        ['id' => 2, 'name' => 'Ahmad Ali','date'=>'2019-11-01', 'att_in' => '7:55','att_out' => '5:00'],
        ['id' => 3, 'name' => 'Muhamad Abu','date'=>'2019-11-01', 'att_in' => '8:00','att_out' => '5:00'],
        ['id' => 4, 'name' => 'Siti Khadijah','date'=>'2019-11-01', 'att_in' => '8:00','att_out' => '5:00'],
        ['id' => 5, 'name' => 'Nur Fairuz','date'=>'2019-11-01', 'att_in' => '8:00','att_out' => '5:00'],
        ['id' => 6, 'name' => 'Siti Aminah','date'=>'2019-11-01', 'att_in' => '8:00','att_out' => '5:00'],
        ['id' => 7, 'name' => 'Muhamad Fadhil','date'=>'2019-11-02', 'att_in' => '8:00','att_out' => '5:00'],
        ['id' => 8, 'name' => 'Ahmad Ali','date'=>'2019-11-02', 'att_in' => '8:00','att_out' => '5:00'],
        ['id' => 9, 'name' => 'Muhamad Abu','date'=>'2019-11-02', 'att_in' => '8:00','att_out' => '5:00'],
        ['id' => 10, 'name' => 'Siti Khadijah','date'=>'2019-11-02', 'att_in' => '8:00','att_out' => '5:00'],
        ['id' => 11, 'name' => 'Nur Fairuz','date'=>'2019-11-02', 'att_in' => '8:00','att_out' => '5:00'],
        ['id' => 12, 'name' => 'Siti Aminah','date'=>'2019-11-02', 'att_in' => '8:00','att_out' => '5:00'],
    ]);
    return view('attendance.index', compact('attendances')); @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'index.blade.php'</p>
    </div>
    @markdown @verbatim
    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Age</th>
                <th>Start date</th>
                <th>Salary</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011/04/25</td>
                <td>$320,800</td>
            </tr>
            <tr>
                <td>Donna Snider</td>
                <td>Customer Support</td>
                <td>New York</td>
                <td>27</td>
                <td>2011/01/25</td>
                <td>$112,000</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Age</th>
                <th>Start date</th>
                <th>Salary</th>
            </tr>
        </tfoot>
    </table>

    Change above code to this code:-

    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                    <th>Id</th>
                <th>Name</th>
                <th>Date</th>
                <th>Clock In</th>
                <th>Clock Out</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($attendances as $attendance)
                <tr>
                    <td>{{$attendance['id']}}</td>
                    <td>{{$attendance['name']}}</td>
                    <td>{{$attendance['date']}}</td>
                    <td>{{$attendance['att_in']}}</td>
                    <td>{{$attendance['att_out']}}</td>
                    <td>
                        <a href="{{route('attendances.show',['attendance'=>$attendance['id']])}}">View</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                    <th>Id</th>
                <th>Name</th>
                <th>Date</th>
                <th>Clock In</th>
                <th>Clock Out</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table> @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent