@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code inside route 'page/2'</p>
    </div>
    @markdown @verbatim
    Route::get('page/2', function () {
        App::setLocale(Request()->lang);
        return view('page.2');
    })->name('page.2'); @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code at url</p>
    </div>
    @markdown @verbatim
    ?lang=ms @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent