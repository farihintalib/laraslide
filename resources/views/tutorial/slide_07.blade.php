@component('layouts.components.timeline_item',['i'=>$i,'color'=>'bg-aqua']) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <h4>via Laravel installer</h4>
    @markdown @verbatim
    composer global require laravel/installer @endverbatim @endmarkdown
    @markdown @verbatim
    laravel new laravel @endverbatim @endmarkdown
    @markdown @verbatim
    php artisan -v @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent