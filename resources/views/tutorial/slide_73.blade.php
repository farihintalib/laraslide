@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code inside UserController@update</p>
    </div>
    @markdown @verbatim
    public function update(Request $request, User $user) {
        $input = $request->all();
        $user->fill($input);
        $user->save();
        return redirect(back()->getTargetUrl());
    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent