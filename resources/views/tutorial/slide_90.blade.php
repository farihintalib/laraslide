@component('layouts.components.timeline_item',['i'=>$i,'color'=>'bg-aqua']) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Change this code at \resources\views\welcome.blade.php folder</p>
    </div>
    @markdown @verbatim
    <title>{{config('app.name')}}</title> @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent