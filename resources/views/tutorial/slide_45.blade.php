@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code inside 'edit.blade.php'</p>
    </div>
    @markdown @verbatim
    <div class="content">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="m-b-md">
            <form method="POST" action="{{ route('attendances.update',['attendance'=>$attendance['id']]) }}">
                @csrf
                @method('PUT')
                Id: <input type="integer" name="id" value="{{$attendance['id']}}"> <br/>
                Name: <input type="text" name="name" value="{{$attendance['name']}}"> <br/>
                Date: <input type="text" name="date" value="{{$attendance['date']}}"> <br/>
                Clock In: <input type="text" name="att_in" value="{{$attendance['att_in']}}"> <br/>
                Clock Out: <input type="text" name="att_out" value="{{$attendance['att_out']}}"> <br/>
                <button type="submit">Update</button>
            </form>
        </div>
        <div class="links">
            <a href="https://laravel.com/docs">Docs</a>
            <a href="https://laracasts.com">Laracasts</a>
            <a href="https://laravel-news.com">News</a>
            <a href="https://blog.laravel.com">Blog</a>
            <a href="{{url('tutorial')}}">Tutorial</a>
            <a href="https://github.com/laravel/laravel">GitHub</a>
        </div>
    </div> @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside AttendanceController@update</p>
    </div>
    @markdown @verbatim
    public function update(Request $request, $id) {
        $request->validate([
            'id' => 'required|integer',
            'name' => 'required|max:10',
            'date' => 'required|date',
            'att_in' => 'required',
            'att_out' => 'required',
        ]);
        $attendances = session('attendances');
        if (isset($attendances)) {
            $attendances = $attendances->filter(function ($value)use($id) {
                return $value['id'] != $id;
            });
            $attendances = $attendances->merge([['id' => $request->id, 'name' => $request->name, 'date' => $request->date, 'att_in' => $request->att_in, 'att_out' => $request->att_out]]);
            Request()->session()->put('attendances', $attendances);
            return redirect(route('attendances.index'));
        } else {
            return abort(404);
        }
    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent