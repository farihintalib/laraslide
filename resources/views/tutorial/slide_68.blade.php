@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code inside route</p>
    </div>
    @markdown @verbatim
    Route::group(['middleware' => ['auth']], function () {
        Route::resource('users', 'UserController');
    }); @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Scaffold UserController with model. Write this code inside command</p>
    </div>
    @markdown @verbatim
    php artisan make:controller UserController --resource --model=User @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside UserController@index</p>
    </div>
    @markdown @verbatim
    public function index()
    {
        $users = User::all();
        return view('user.index', compact('users'));
    } @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'user/index.blade.php'</p>
    </div>
    @markdown @verbatim
    @extends('layouts.app')

    @section('content-header')
        <h1>
            Users
            <small>Laravel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Users</li>
        </ol>
    @endsection

    @section('main-content')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Users</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    <a href="{{route('users.show',['user'=>$user->id])}}">View</a>
                                    <a href="{{route('users.edit',['user'=>$user->id])}}">Edit</a>
                                    <form id="user{{$user->id}}" method="POST" action="{{route('users.destroy',['user'=>$user->id])}}">
                                        @csrf
                                        @method('DELETE')
                                        <a href="javascript:void()" onclick="document.getElementById('user{{$user->id}}').submit()">Delete</a>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        @push('scripts')
            <script>
                $(document).ready(function() {
                    $('#example').DataTable();
                });
            </script>
        @endpush
    @endsection @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'layouts/scripts.blade.php'</p>
    </div>
    @markdown @verbatim
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> @endverbatim @endmarkdown

    <div class="callout callout-success">
        <p>Write this code inside 'layouts/styles.blade.php'</p>
    </div>
    @markdown @verbatim
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"> @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Write this code inside 'layouts/menu.blade.php'</p>
    </div>
    @markdown @verbatim
    @component('layouts.components.nav_li',['url'=>url('users'),'class'=>'fa fa-circle-o','label'=>'Users']) @endcomponent @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent