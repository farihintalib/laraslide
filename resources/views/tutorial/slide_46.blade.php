@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <object class="img-responsive" type="image/svg+xml" data="{{url('/images/tutorial/picture25.svg')}}"></object>
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://laravel.com/docs/9.x/errors">Errors Documentation</a></li>
                    <li><a href="https://laravel.com/docs/9.x/logging">Logging Documentation</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent