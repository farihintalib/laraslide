@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code inside UserController@show</p>
    </div>
    @markdown @verbatim
    public function show(User $user)
    {
        return view('user.show', compact('user'));
    } @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'user/show.blade.php'</p>
    </div>
    @markdown @verbatim
    @extends('layouts.app')

    @section('content-header')
        <h1>
            User
            <small>Laravel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User</li>
        </ol>
    @endsection

    @section('main-content')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">User</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <p>
                    <strong>Name:</strong> {{$user->name}}<br>
                    <strong>Email:</strong> {{$user->email}}
                </p>
            </div>
        </div>
    @endsection @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent