@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')

    <div class="callout callout-success">
        <p>Controller</p>
    </div>
    
    @markdown @verbatim
    public function index(Request $request) {
        if ($request->ajax()) {
            return response($this->data($request->type));
        }
        return view('chart.index');
    }

    protected function data($type) {
        switch ($type) {
            case "line":
                return [
                    'data' => [10, 41, 35, 51, 49, 62, 69, 91, 100],
                    'categories' => ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep']
                ];
            case "area":
                return [
                    'series' => [
                        [
                            'name' => 'series1',
                            'data' => [31, 40, 28, 51, 42, 109, 100],
                        ],
                        [
                            'name' => 'series2',
                            'data' => [11, 32, 45, 32, 34, 52, 41]
                        ],
                    ],
                ];
        }
    } @endverbatim @endmarkdown

    <div class="callout callout-success">
        <p>View (JS)</p>
    </div>
    
    @markdown @verbatim
    var line = function (el) {
            $.ajax({
                url: "/charts?type=line",
                method: "GET",
                success: function (response) {
                    var options = {
                        series: [{
                                name: "Desktops",
                                data: response.data
                            }],
                        chart: {
                            height: 350,
                            type: 'line',
                            zoom: {
                                enabled: false
                            }
                        },
                        dataLabels: {
                            enabled: false
                        },
                        stroke: {
                            curve: 'straight'
                        },
                        title: {
                            text: 'Product Trends by Month',
                            align: 'left'
                        },
                        grid: {
                            row: {
                                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                                opacity: 0.5
                            },
                        },
                        xaxis: {
                            categories: response.categories
                        }
                    };
                    var chart = new ApexCharts(document.querySelector(el), options);
                    chart.render();
                }
            });
        };
        var area = function (el) {
            $.ajax({
                url: "/charts?type=area",
                method: "GET",
                success: function (response) {
                    var options = {
                        series: response.series,
                        chart: {
                            height: 350,
                            type: 'area'
                        },
                        dataLabels: {
                            enabled: false
                        },
                        stroke: {
                            curve: 'smooth'
                        },
                        xaxis: {
                            type: 'datetime',
                            categories: ["2018-09-19T00:00:00.000Z", "2018-09-19T01:30:00.000Z", "2018-09-19T02:30:00.000Z", "2018-09-19T03:30:00.000Z", "2018-09-19T04:30:00.000Z", "2018-09-19T05:30:00.000Z", "2018-09-19T06:30:00.000Z"]
                        },
                        tooltip: {
                            x: {
                                format: 'dd/MM/yy HH:mm'
                            },
                        },
                    };
                    var chart = new ApexCharts(document.querySelector(el), options);
                    chart.render();
                }
            });
        }; @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://apexcharts.com/docs/installation">Apexcharts</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent