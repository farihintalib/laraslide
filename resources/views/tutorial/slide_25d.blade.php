@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Make middleware</p>
    </div>
    @markdown @verbatim
    php artisan make:middleware Devtool @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Write this code into middleware</p>
    </div>
    @markdown @verbatim
    public function handle($request, Closure $next) {
        if (!empty($request->email ?? null) && $request->email == 'admin@admin.admin') {
            return $next($request);
        } else {
            abort(404);
        }
    } @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Register the middleware at kernel.php</p>
    </div>
    
    <div class="callout callout-success">
        <p>Route</p>
    </div>
    @markdown @verbatim
    Route::group([
    'prefix' => 'devtool',
    'as' => 'devtool.'
        ], function () {
            Route::group(['middleware' => ['devtool']], function () {
                Route::get('debug', [Controllers\Devtool\DebugController::class, 'index'])->name('debug');
                Route::get('optimize', [Controllers\Devtool\DebugController::class, 'optimize'])->name('optimize');
            });
        }); @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Controller</p>
    </div>
    @markdown @verbatim
    namespace App\Http\Controllers\Devtool;

    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Artisan;

    class DebugController extends Controller {

        public function index() {
            dd('This is index');
        }

        public function optimize() {
            $command = "config:cache";
            Artisan::call($command);
            dd('successful');
        }

    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent