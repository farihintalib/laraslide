@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <object class="img-responsive" type="image/svg+xml" data="{{url('/images/tutorial/85.svg')}}"></object>
@endslot @slot('timeline_footer')
@endslot @endcomponent