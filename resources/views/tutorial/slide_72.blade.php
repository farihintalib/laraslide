@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code inside UserController@edit</p>
    </div>
    @markdown @verbatim
    public function edit(User $user) {
        return view('user.edit', compact('user'));
    } @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'user/edit.blade.php'</p>
    </div>
    @markdown @verbatim
    @extends('layouts.app')

    @section('content-header')
        <h1>
            User
            <small>Laravel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User</li>
        </ol>
    @endsection

    @section('main-content')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">User</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <p>
                    <form method="POST" action="{{ route('users.update',['user'=>$user->id]) }}">
                        @csrf
                        @method('PUT')
                        <strong>Name:</strong> <input type="text" name="name" value="{{$user->name}}" required maxlength="254" placeholder="First name"><br/>
                        <strong>Email:</strong> <input type="email" name="email" value="{{$user->email}}" required placeholder="Email"><br/>
                        <button type="submit">Update</button>
                    </form>
                </p>
            </div>
        </div>
    @endsection @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent