@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Inspect request in function controller</p>
    </div>
    @markdown @verbatim
    dd(Request(),Request()->all()); @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside GreetingController@index</p>
    </div>
    @markdown @verbatim
    public function index() {
        $name = request()->name;
        if (isset($name)) {
            return $this->greeting . 'World & ' . $name;
        } else {
            return $this->greeting . 'World';
        }
    } @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Add input at url</p>
    </div>
    @markdown @verbatim
    ?name=Ahmad Ali @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Modified the code</p>
    </div>
    @markdown @verbatim
    public function index(Request $request) {
        $name = $request->name;
        if (isset($name)) {
            return $this->greeting . 'World & ' . $name;
        } else {
            return $this->greeting . 'World';
        }
    } @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Lets see if the input is array</p>
    </div>
    @markdown @verbatim
    ?name[]=Ali&name[]=Abu @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Modified the code</p>
    </div>
    @markdown @verbatim
    public function index(Request $request) {
        $name = $request->name;
        if (!empty($name ?? null)) {
            if (is_array($name)) {
                $name = implode(' & ', $name);
                return $this->greeting . 'World & ' . $name;
            } else {
                return $this->greeting . 'World & ' . $name;
            }
        } else {
            return $this->greeting . 'World';
        }
    } @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Continue some example in full documentation</p>
    </div>
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://laravel.com/docs/9.x/requests">Full Documentation</a></li>
                    <li><a href="https://www.php.net/manual/en/function.implode.php">Implode</a></li>
                    <li><a href="https://www.php.net/manual/en/function.explode.php">Explode</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent