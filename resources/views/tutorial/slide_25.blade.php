@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code using CLI</p>
    </div>
    @markdown @verbatim
    php artisan make:controller GreetingController @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside Route</p>
    </div>
    @markdown @verbatim
    Route::get('greeting', 'GreetingController@index')->name('greeting.index');
    Route::get('greeting/{name}', 'GreetingController@show')->name('greeting.show'); @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside controller</p>
    </div>
    @markdown @verbatim
    public function index() {
        return 'Hello World';
    }

    public function show($name) {
        return 'Hello ' . $name;
    } @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Modified above code to below code inside same controller</p>
    </div>
    @markdown @verbatim
    protected $greeting;
    
    public function __construct() {
        $this->greeting = 'Hello ';
        $this->middleware('auth');
    //    $this->middleware('auth')->except('show');
    //    $this->middleware('auth')->only('show');
    }

    public function index() {
        return $this->greeting . 'World';
    }

    public function show($name) {
        return $this->greeting . $name;
    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')

@endslot @endcomponent