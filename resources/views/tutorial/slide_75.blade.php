@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Create new component 'layouts.components.session_message.blade.php'. Write this code inside 'session_message.blade.php'</p>
    </div>
    @markdown @verbatim
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
    @if(Session::has('success'))
        <p id="successMessage" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success') }}</p>
    @endif
    @if(Session::has('error'))
        <p id="successMessage" class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
    @endif
    @if(Session::has('validator-error'))
        <p id="successMessage" class="alert {{ Session::get('alert-class', 'alert-danger') }}">
            @foreach (Session::get('validator-error')->all() as $error)
                {!! $error !!}<br>
            @endforeach
        </p>
    @endif @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'user/create.blade.php', 'user/edit.blade.php', 'user/index.blade.php' and 'user/show.blade.php' after class="box-body"</p>
    </div>
    @markdown @verbatim
    @include('layouts.components.session_message') @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Write this code inside UserController@store, UserController@update and UserController@destroy</p>
    </div>
    @markdown @verbatim
    \Session::flash('success', 'Successfully Created');
    \Session::flash('success', 'Successfully Updated');
    \Session::flash('success', 'Successfully Deleted'); @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent