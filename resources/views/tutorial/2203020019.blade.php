@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Controller</p>
    </div>
    @markdown @verbatim
    use App\Models\Attendance;
    
    case "donut":
        $lateCount = Attendance::where('status', 'late')->count();
        $absentCount = Attendance::where('status', 'absent')->count();
        $presentCount = Attendance::where('status', 'present')->count();
        return [
            'labels' => ['Late', 'Absent', 'Present'],
            'series' => [$lateCount, $absentCount, $presentCount],
        ]; @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>JS</p>
    </div>
    
    @markdown @verbatim
    var donut = function (el) {
        $.ajax({
            url: "/adminlte/charts?type=donut",
            method: "GET",
            success: function (response) {
                var options = {
                    series: response.series,
                    chart: {
                        width: 380,
                        type: 'pie',
                    },
                    labels: response.labels,
                    responsive: [{
                            breakpoint: 480,
                            options: {
                                chart: {
                                    width: 200
                                },
                                legend: {
                                    position: 'bottom'
                                }
                            }
                        }]
                };
                var chart = new ApexCharts(document.querySelector(el), options);
                chart.render();
            }
        });
    }; @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent