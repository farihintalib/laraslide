@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <object class="img-responsive" type="image/svg+xml" data="{{url('/images/tutorial/picture24.svg')}}"></object>
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://laravel.com/docs/9.x/validation">Full Documentation - Backend</a></li>
                    <li><a href="https://www.w3schools.com/html/html_form_attributes.asp">Full Documentation - Frontend</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent