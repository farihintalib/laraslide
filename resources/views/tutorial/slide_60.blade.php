@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Install laravel/ui via composer. Write this code inside command</p>
    </div>
    @markdown @verbatim
    composer require laravel/ui 
    
    or
    
    composer require laravel/ui:^3.0 @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Scaffold authentication. Write this code inside command</p>
    </div>
    @markdown @verbatim
    php artisan ui bootstrap --auth @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Required node_module JS. Write this code inside command. Backup file if prompted file already exist then select 'yes'.</p>
    </div>
    @markdown @verbatim
    npm install @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Compiling assets. Write this code inside command.</p>
    </div>
    @markdown @verbatim
    npm run dev @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://www.npmjs.com">NPM Package</a></li>
                    <li><a href="https://nodejs.org/en/download/">NodeJs installer</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent