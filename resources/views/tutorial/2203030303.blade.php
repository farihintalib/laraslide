@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>CLI</p>
    </div>
    @markdown @verbatim
    php artisan make:migration add_first_name_to_users_table @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Schema</p>
    </div>
    @markdown @verbatim
    public function up() {
        Schema::table('users', function (Blueprint $table) {
            $table->string('last_name')->after('name');
            $table->string('first_name')->after('name');
        });
    }

    public function down() {
        Schema::table('users', function (Blueprint $table) {
            $table->drop('first_name');
            $table->drop('last_name');
        });
    } @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>CLI</p>
    </div>
    @markdown @verbatim
    php artisan migrate @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Model (User)</p>
    </div>
    @markdown @verbatim
    public function getFullNameAttribute(){
        return "{$this->first_name} {$this->last_name}";
    } @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>View</p>
    </div>
    @markdown @verbatim
    auth()->user()->full_name @endverbatim @endmarkdown
    
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://laravel.com/docs/9.x/eloquent-mutators">Mutators</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent