@component('layouts.components.timeline_item',['i'=>$i,'color'=>'bg-aqua']) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code at \route folder</p>
    </div>
    @markdown @verbatim
    Route::group(['prefix' => 'prefix', 'as' => 'as.'], function () {
        Route::get('/hello_world', function () {
            return 'Hello World';
        })->name('hello_world');
    }); @endverbatim @endmarkdown
    @markdown @verbatim
    php artisan route:list @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent