@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>composer require psr/simple-cache:^1.0 maatwebsite/excel</p>
    </div>
    <div class="callout callout-success">
        <p>php artisan vendor:publish --provider="Maatwebsite\Excel\ExcelServiceProvider" --tag=config</p>
    </div>
    <div class="callout callout-success">
        <p>php artisan config:cache</p>
    </div>
    <div class="callout callout-success">
        <p>php artisan make:import UsersImport</p>
    </div>
    @markdown @verbatim
    namespace App\Imports;

    use Illuminate\Support\Collection;
    use Maatwebsite\Excel\Concerns\ToCollection;
    use Maatwebsite\Excel\Concerns\WithHeadingRow;

    class UsersImport implements ToCollection, WithHeadingRow {

        /**
         * @param Collection $collection
         */
        public function collection(Collection $collection) {
            dd($collection);
        }

        /**
         * With Heading Row
         *
         * @return integer
         */
        public function headingRow(): int {
            return 1;
        }

    } @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Route</p>
    </div>
    @markdown @verbatim
    Route::post('uploads/excel', [Controllers\UploadController::class, 'storeExcel'])->name('uploads.storeExcel'); @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>View</p>
    </div>
    @markdown @verbatim
    <form method="post" action="{{route('uploads.storeExcel')}}" enctype="multipart/form-data">
        @csrf
        <input name="excel" type="file">
        <button type='submit'>Save</button>
    </form> @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Controller</p>
    </div>
    @markdown @verbatim
    public function storeExcel(Request $request) {
        if ($request->hasFile('excel')) {
            Excel::import(new UsersImport, $request->file('excel'));
        }
        dd($request->all());
        return redirect()->back();
    } @endverbatim @endmarkdown
    @markdown @verbatim
    use App\Imports\UsersImport;
    use Maatwebsite\Excel\Facades\Excel; @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://docs.laravel-excel.com/3.1/getting-started/installation.html">Full Documentation</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent