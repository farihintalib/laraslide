@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Create new component 'layouts.components.modal_delete.blade.php'. Write this code inside 'modal_delete.blade.php'</p>
    </div>
    @markdown @verbatim
    <div id="modal-delete" class="modal fade" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header bg-red">
                        <button aria-label="Close" data-dismiss="modal" class="close" type="button">
                            <span aria-hidden="true"> x </span></button>
                        <h4 class="modal-title">Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure to delete? Proceed ?</p>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default pull-left" type="button">Close</button>
                        <button id="deleteButton" class="btn btn-danger" type="submit">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            $(document).on("click", "a[data-modal]", function(e){
                e.preventDefault();
                var route = $(this).data("route");
                $("#modal-delete form").attr("action", route); //id hash, div/form/p tag
            });
        </script>
    @endpush @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'layouts/app.blade.php' after class="wrapper"</p>
    </div>
    @markdown @verbatim
    @stack('modals') @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'user/index.blade.php' before class="box"</p>
    </div>
    @markdown @verbatim
    @push('modals')
        @include('layouts.components.modal_delete')
    @endpush @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside UserController@ajax</p>
    </div>
    @markdown @verbatim
    $btnDelete = "<a href='#' data-modal data-route='" . route('users.destroy', $query->getKey()) . "' data-toggle='modal' data-target='#modal-delete'>Delete</a>"; @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent