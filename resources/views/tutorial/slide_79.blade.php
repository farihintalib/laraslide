@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Scaffold new migration to add 'birthday' to 'users' table. Write this code inside command</p>
    </div>
    @markdown @verbatim
    php artisan make:migration add_birthday_to_users_table --table=users @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside '/database/migration/..._add_birthday_to_users_table'</p>
    </div>
    @markdown @verbatim
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->date('birthday')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('birthday');
        });
    } @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside command</p>
    </div>
    @markdown @verbatim
    php artisan migrate @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'User' model</p>
    </div>
    @markdown @verbatim
    protected $fillable = [
        'name', 'email', 'password', 'department_id','birthday',
    ];
    
    protected $dates = ['birthday']; @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'user/edit.blade.php'</p>
    </div>
    @markdown @verbatim
    <strong>Birthday:</strong> <input type="date" name="birthday" value="{{$user->birthday != null ? $user->birthday->format('Y-m-d') : null}}" required placeholder="Birthday"><br/> @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'user/show.blade.php'</p>
    </div>
    @markdown @verbatim
    <strong>Birthday:</strong> {{isset($user->birthday) ? $user->birthday->format('j M Y') : null}} @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://www.w3schools.com/php/func_date_date_format.asp">PHP Date Format</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent