@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Create relationship department and user. Write this code inside 'Department' model</p>
    </div>
    @markdown @verbatim
    public function departmentIdUser() {
        return $this->hasMany('App\User', 'department_id');
    } @endverbatim @endmarkdown

    <div class="callout callout-success">
        <p>Write this code inside 'department/show.blade.php'</p>
    </div>
    @markdown @verbatim
    @extends('layouts.app')

    @section('content-header')
        <h1>
            Department
            <small>Laravel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Department</li>
        </ol>
    @endsection

    @section('main-content')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Department</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('layouts.components.session_message')
                <p>
                    <strong>Name:</strong> {{$department->name}}<br>
                    <strong>List User</strong><br>
                </p>
                <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($department->departmentIdUser as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    <a href="{{route('users.show',['user'=>$user->id])}}">View</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        @push('scripts')
            <script>
                $(document).ready(function() {
                    $('#example').DataTable();
                });
            </script>
        @endpush
    @endsection @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://laravel.com/docs/9.x/eloquent-relationships">Relationship</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent