@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code inside AttendanceController@update</p>
    </div>
    @markdown @verbatim
    public function update(Request $request, $id) {
        $attendances = session('attendances');
        if (isset($attendances)) {
            $attendances = $attendances->filter(function ($value)use($id) {
                return $value['id'] != $id;
            });
            $attendances = $attendances->merge([['id' => $request->id, 'name' => $request->name, 'date' => $request->date, 'att_in' => $request->att_in, 'att_out' => $request->att_out]]);
            Request()->session()->put('attendances', $attendances);
            return redirect(route('attendances.index'));
        } else {
            return abort(404);
        }
    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent