@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>CLI</p>
    </div>
    @markdown @verbatim
    composer require yajra/laravel-datatables @endverbatim @endmarkdown
    @markdown @verbatim
    php artisan vendor:publish --tag=datatables @endverbatim @endmarkdown
    @markdown @verbatim
    php artisan vendor:publish --tag=datatables-buttons @endverbatim @endmarkdown
    @markdown @verbatim
    php artisan datatables:make AttendancesDataTable @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Route</p>
    </div>
    @markdown @verbatim
    Route::get('ajax-attendances', [Controllers\AttendanceController::class,'indexAjax'])->name('ajax-attendances.indexAjax'); @endverbatim @endmarkdown
     
    <div class="callout callout-success">
        <p>Controller</p>
    </div>
    
    @markdown @verbatim
    use App\DataTables\AttendancesDataTable;
    
    public function indexAjax(AttendancesDataTable $dataTable) {
        return $dataTable->render('attendance.index');
    } @endverbatim @endmarkdown
     
    <div class="callout callout-success">
        <p>Yajra Datatable</p>
    </div>
    @markdown @verbatim
    namespace App\DataTables;

    use Yajra\DataTables\Html\Button;
    use Yajra\DataTables\Html\Column;
    use Yajra\DataTables\Html\Editor\Editor;
    use Yajra\DataTables\Html\Editor\Fields;
    use Yajra\DataTables\Services\DataTable;

    class AttendancesDataTable extends DataTable {

        /**
         * Build DataTable class.
         *
         * @param mixed $query Results from query() method.
         * @return \Yajra\DataTables\DataTableAbstract
         */
        public function dataTable($query) {
            return datatables()
                            ->of($query)
                            ->addColumn('action', function($query){
                                return '<a href="'.route('attendances.show',['attendance'=>$query['id']]).'">View</a>';
                            })
                            ->rawColumns(['action']);
        }

        /**
         * Get query source of dataTable.
         *
         * @param \App\Models\AttendancesDataTable $model
         * @return \Illuminate\Database\Eloquent\Builder
         */
        public function query() {
            return [
                ['id' => 1, 'name' => 'Muhamad Fadhil', 'date' => '2019-11-01', 'att_in' => '8:00', 'att_out' => '5:00'],
                ['id' => 2, 'name' => 'Ahmad Ali', 'date' => '2019-11-01', 'att_in' => '7:55', 'att_out' => '5:00'],
                ['id' => 3, 'name' => 'Muhamad Abu', 'date' => '2019-11-01', 'att_in' => '8:00', 'att_out' => '5:00'],
                ['id' => 4, 'name' => 'Siti Khadijah', 'date' => '2019-11-01', 'att_in' => '8:00', 'att_out' => '5:00'],
                ['id' => 5, 'name' => 'Nur Fairuz', 'date' => '2019-11-01', 'att_in' => '8:00', 'att_out' => '5:00'],
                ['id' => 6, 'name' => 'Siti Aminah', 'date' => '2019-11-01', 'att_in' => '8:00', 'att_out' => '5:00'],
                ['id' => 7, 'name' => 'Muhamad Fadhil', 'date' => '2019-11-02', 'att_in' => '8:00', 'att_out' => '5:00'],
                ['id' => 8, 'name' => 'Ahmad Ali', 'date' => '2019-11-02', 'att_in' => '8:00', 'att_out' => '5:00'],
                ['id' => 9, 'name' => 'Muhamad Abu', 'date' => '2019-11-02', 'att_in' => '8:00', 'att_out' => '5:00'],
                ['id' => 10, 'name' => 'Siti Khadijah', 'date' => '2019-11-02', 'att_in' => '8:00', 'att_out' => '5:00'],
                ['id' => 11, 'name' => 'Nur Fairuz', 'date' => '2019-11-02', 'att_in' => '8:00', 'att_out' => '5:00'],
                ['id' => 12, 'name' => 'Siti Aminah', 'date' => '2019-11-02', 'att_in' => '8:00', 'att_out' => '5:00'],
            ];
        }

        /**
         * Optional method if you want to use html builder.
         *
         * @return \Yajra\DataTables\Html\Builder
         */
        public function html() {
            return $this->builder()
                            ->setTableId('attendancesdatatable-table')
                            ->columns($this->getColumns())
                            ->minifiedAjax()
                            ->dom('Bfrtip')
                            ->orderBy(0,'asc')
                            ->buttons(
                                    Button::make('create'),
                                    Button::make('export'),
                                    Button::make('print'),
                                    Button::make('reset'),
                                    Button::make('reload')
            );
        }

        /**
         * Get columns.
         *
         * @return array
         */
        protected function getColumns() {
            return [
                Column::make('id')->width(60)->addClass('text-center'),
                Column::make('name'),
                Column::make('date'),
                Column::make('att_in'),
                Column::make('att_out'),
                Column::make('action'),
            ];
        }

        /**
         * Get filename for export.
         *
         * @return string
         */
        protected function filename() {
            return 'Attendances_' . date('YmdHis');
        }

    } @endverbatim @endmarkdown
     
    <div class="callout callout-success">
        <p>View</p>
    </div>
    @markdown @verbatim
    <!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <title>Laravel</title>
            <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
            <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
            <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
            <style>
                html, body {
                    background-color: #fff;
                    color: #636b6f;
                    font-family: 'Nunito', sans-serif;
                    font-weight: 200;
                    height: 100vh;
                    margin: 0;
                }
                .full-height {
                    height: 100vh;
                }
                .flex-center {
                    align-items: center;
                    display: flex;
                    justify-content: center;
                }
                .position-ref {
                    position: relative;
                }
                .top-right {
                    position: absolute;
                    right: 10px;
                    top: 18px;
                }
                .content {
                    text-align: center;
                }
                .title {
                    font-size: 84px;
                }
                .links > a {
                    color: #636b6f;
                    padding: 0 25px;
                    font-size: 13px;
                    font-weight: 600;
                    letter-spacing: .1rem;
                    text-decoration: none;
                    text-transform: uppercase;
                }
                .m-b-md {
                    margin-bottom: 30px;
                }
            </style>
        </head>
        <body>
            <div class="flex-center position-ref">
                @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                    <a href="{{ url('/home') }}">Home</a>
                    @else
                    <a href="{{ route('login') }}">Login</a>
                    @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                    @endif
                    @endauth
                </div>
                @endif
                <div class="content" style="margin-top: 50px;">
                    <div class="m-b-md">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
            <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
            <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
            <script src="{{url('vendor/datatables/buttons.server-side.js')}}"></script>
            {!! $dataTable->scripts() !!}
        </body>
    </html> @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://yajrabox.com/docs/laravel-datatables/master/installation">Yajra Datatables</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent