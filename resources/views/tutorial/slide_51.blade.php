@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Create new 'layouts\components\nav_li.blade.php'. Write this code inside 'nav_li.blade.php'</p>
    </div>
    @markdown @verbatim
    <li><a href="{{ $url ?? null}}"><i class="{{ $class ?? null}}"></i> <span>{{ $label ?? null}}</span></a></li> @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'layouts\menu.blade.php'</p>
    </div>
    @markdown @verbatim
    @component('layouts.components.nav_li',['url'=>url('page/1')]) @slot('class')
        fa fa-circle-o
    @endslot @slot('label')
        Page 1
    @endslot @endcomponent
    @component('layouts.components.nav_li',['url'=>url('page/2'),'class'=>'fa fa-circle-o','label'=>'Page 2']) @endcomponent
    @component('layouts.components.nav_li',['url'=>url('page/3'),'class'=>'fa fa-circle-o','label'=>'Page 3']) @endcomponent @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent