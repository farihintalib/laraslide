@component('layouts.components.timeline_item',['i'=>$i,'color'=>'bg-aqua']) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <img class="img-responsive" src="https://laraslide.test/images/tutorial/Picture40.png">
    <br>
    @markdown @verbatim
    composer install @endverbatim @endmarkdown
    @markdown @verbatim
    composer update @endverbatim @endmarkdown
    @markdown @verbatim
    composer dump-autoload @endverbatim @endmarkdown
    @markdown @verbatim
    composer require doctrine/dbal @endverbatim @endmarkdown
    @markdown @verbatim
    composer remove doctrine/dbal @endverbatim @endmarkdown
    @markdown @verbatim
    "autoload": {
        "psr-4": {
            "App\\": "app/",
            "Database\\Factories\\": "database/factories/",
            "Database\\Seeders\\": "database/seeders/"
        },
        "files": [
            "app/Helpers/Helper.php",
        ]
    }, @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code at file app/Helpers/Helper.php</p>
    </div>
    @markdown @verbatim
    if (!function_exists('helloWorld')) {
        function helloWorld() {
            dd('Hello World');
            //echo 'Hello World';
        }
    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://packagist.org">More package</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent