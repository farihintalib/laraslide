@component('layouts.components.timeline_item',['i'=>$i,'color'=>'bg-aqua']) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code at \route folder</p>
    </div>
    @markdown @verbatim
    Route::get('/hello/{name}', function ($name) {
        return 'Hello ' . $name;
    })->name('hello_name'); @endverbatim @endmarkdown
    @markdown @verbatim
    Route::get('/hello/{firstname}/{lastname}', function ($firstname, $lastname) {
        $fullname = $firstname . ' ' . $lastname;
        return 'Hello ' . $fullname;
    })->name('hello_fullname'); @endverbatim @endmarkdown
    @markdown @verbatim
    Route::get('/user/{id}', function ($id) {
        $users = collect([
            ['id' => 1, 'firstname' => 'Muhamad Fadhil', 'lastname' => 'Faisal', 'age' => 25],
            ['id' => 2, 'firstname' => 'Ahmad Ali', 'lastname' => 'Bakar', 'age' => 19],
            ['id' => 3, 'firstname' => 'Khadijah', 'lastname' => 'Abu', 'age' => 23],
            ['id' => 4, 'firstname' => 'Siti Aminah', 'lastname' => 'Yusof', 'age' => 28]
        ]);
        $user = $users->firstWhere('id', $id);
    //    if(!empty($user ?? null)){
    //        $fullname = $user['firstname'] . ' ' . $user['lastname'];
    //        return '<p><strong>Name:</strong> ' . $fullname . '<br><strong>Age:</strong> ' . $user['age'] . '</p>';
    //    }else{
    //        return abort(404);
    //    }
        if (isset($user)) {
            $fullname = $user['firstname'] . ' ' . $user['lastname'];
            return '<p><strong>Name:</strong> ' . $fullname . '<br><strong>Age:</strong> ' . $user['age'] . '</p>';
        } else {
            return abort(404);
        }
    })->name('user_profile'); @endverbatim @endmarkdown
    @markdown @verbatim
    Route::get('/profile', function ($firstname = 'Ahmad Ali', $lastname = 'Bakar', $age = 23) {
    //  $fullname = $firstname . ' ' . $lastname;
        $fullname = "{$firstname} {$lastname}";
        return '<p><strong>Name:</strong> ' . $fullname . '<br><strong>Age:</strong> ' . $age . '</p>';
    })->name('profile'); @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://laravel.com/docs/9.x/collections">Collection</a></li>
                    <li><a href="https://www.php.net/manual/en/function.isset.php">Isset</a></li>
                    <li><a href="https://www.php.net/manual/en/function.empty.php">Empty</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent