@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>CLI</p>
    </div>
    @markdown @verbatim
    php artisan make:component Adminlte\Navli @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Controller</p>
    </div>
    @markdown @verbatim
    namespace App\View\Components\Adminlte;

    use Illuminate\View\Component;

    class Navli extends Component {

        public $url;
        public $icon;

        /**
         * Create a new component instance.
         *
         * @return void
         */
        public function __construct($url, $icon = null) {
            $this->url = $url;
            $this->icon = $icon;
        }

        /**
         * Get the view / contents that represent the component.
         *
         * @return \Illuminate\Contracts\View\View|\Closure|string
         */
        public function render() {
            return view('components.adminlte.navli');
        }

    } @endverbatim @endmarkdown
     
    <div class="callout callout-success">
        <p>View (Component)</p>
    </div>
    
    @markdown @verbatim
    <li><a href="{{ $url ?? null}}"><i class="{{ $icon ?? null}}"></i> <span>{{ $slot }}</span></a></li> @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>View (Menu)</p>
    </div>
    
    @markdown @verbatim
    <x-adminlte.navli url="{{route('adminlte.index2')}}" icon="fa fa-circle-o">Dashboard v2</x-adminlte.navli> @endverbatim @endmarkdown
     
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://laravel.com/docs/9.x/blade">Blade</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent