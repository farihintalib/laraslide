@component('layouts.components.timeline_item',['i'=>$i,'color'=>'bg-aqua']) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <object class="img-responsive" type="image/svg+xml" data="{{url('/images/tutorial/08.svg')}}"></object>
    <br />
    @markdown @verbatim
    php artisan serve @endverbatim @endmarkdown
    @markdown @verbatim
    php artisan serve --port=3232 @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent