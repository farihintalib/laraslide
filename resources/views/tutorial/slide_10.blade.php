@component('layouts.components.timeline_item',['i'=>$i,'color'=>'bg-aqua']) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    @markdown @verbatim
    php artisan make:controller UploadController @endverbatim @endmarkdown
    @markdown @verbatim
    php artisan make:controller PermissionGroupController @endverbatim @endmarkdown
    @markdown @verbatim
    php artisan make:model Upload @endverbatim @endmarkdown
    @markdown @verbatim
    php artisan make:model PermissionGroup @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent