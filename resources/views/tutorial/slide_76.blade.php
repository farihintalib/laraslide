@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Scaffold new database 'departments' table. Write this code inside command</p>
    </div>
    @markdown @verbatim
    php artisan make:migration create_departments_table @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside '/database/migration/..._create_departments_table'</p>
    </div>
    @markdown @verbatim
    Schema::create('departments', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('name');
        $table->timestamps();
    }); @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside command</p>
    </div>
    @markdown @verbatim
    php artisan migrate @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside route under auth middleware</p>
    </div>
    @markdown @verbatim
    Route::resource('departments', 'DepartmentController'); @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Scaffold new resource controller and model. Write this code inside command</p>
    </div>
    @markdown @verbatim
    php artisan make:controller DepartmentController --resource --model=Department @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Allow fill to column db. Write this code inside 'Department' model</p>
    </div>
    @markdown @verbatim
    protected $fillable = [
        'name'
    ]; @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>CRUD function. Write this code inside 'DepartmentController'</p>
    </div>
    @markdown @verbatim
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $departments = Department::all();
        return view('department.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $input = $request->all();
        $department = new Department();
        $department->fill($input);
        $department->save();
        \Session::flash('success', 'Successfully Created');
        return redirect(route('departments.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department) {
        return view('department.show', compact('department'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department) {
        return view('department.edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department) {
        $input = $request->all();
        $department->fill($input);
        $department->save();
        \Session::flash('success', 'Successfully Updated');
        return redirect(back()->getTargetUrl());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department) {
        $department->delete();
        \Session::flash('success', 'Successfully Deleted');
        return redirect(back()->getTargetUrl());
    } @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'department/index.blade.php'</p>
    </div>
    @markdown @verbatim
    @extends('layouts.app')

    @section('content-header')
        <h1>
            Departments
            <small>Laravel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Departments</li>
        </ol>
    @endsection

    @section('main-content')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Departments</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('layouts.components.session_message')
                <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($departments as $department)
                            <tr>
                                <td>{{$department->id}}</td>
                                <td>{{$department->name}}</td>
                                <td>
                                    <a href="{{route('departments.show',['department'=>$department->id])}}">View</a>
                                    <a href="{{route('departments.edit',['department'=>$department->id])}}">Edit</a>
                                    <form id="department{{$department->id}}" method="POST" action="{{route('departments.destroy',['department'=>$department->id])}}">
                                        @csrf
                                        @method('DELETE')
                                        <a href="javascript:void()" onclick="document.getElementById('department{{$department->id}}').submit()">Delete</a>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        @push('scripts')
            <script>
                $(document).ready(function() {
                    $('#example').DataTable();
                });
            </script>
        @endpush
    @endsection @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'department/create.blade.php'</p>
    </div>
    @markdown @verbatim
    @extends('layouts.app')

    @section('content-header')
        <h1>
            Department
            <small>Laravel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Department</li>
        </ol>
    @endsection

    @section('main-content')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Department</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('layouts.components.session_message')
                <p>
                    <form method="POST" action="{{ route('departments.store') }}">
                        @csrf
                        <strong>Name:</strong> <input type="text" name="name" required maxlength="254" placeholder="Name"><br/>
                        <button type="submit">Create</button>
                    </form>
                </p>
            </div>
        </div>
    @endsection @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'department/show.blade.php'</p>
    </div>
    @markdown @verbatim
    @extends('layouts.app')

    @section('content-header')
        <h1>
            Department
            <small>Laravel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Department</li>
        </ol>
    @endsection

    @section('main-content')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Department</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('layouts.components.session_message')
                <p>
                    <strong>Name:</strong> {{$department->name}}<br>
                </p>
            </div>
        </div>
    @endsection @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'department/edit.blade.php'</p>
    </div>
    @markdown @verbatim
    @extends('layouts.app')

    @section('content-header')
        <h1>
            Department
            <small>Laravel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Department</li>
        </ol>
    @endsection

    @section('main-content')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Department</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('layouts.components.session_message')
                <p>
                    <form method="POST" action="{{ route('departments.update',['department'=>$department->id]) }}">
                        @csrf
                        @method('PUT')
                        <strong>Name:</strong> <input type="text" name="name" value="{{$department->name}}" required maxlength="254" placeholder="Name"><br/>
                        <button type="submit">Update</button>
                    </form>
                </p>
            </div>
        </div>
    @endsection @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Write this code inside 'layouts/menu.blade.php'</p>
    </div>
    @markdown @verbatim
    @component('layouts.components.nav_li',['url'=>url('departments'),'class'=>'fa fa-circle-o','label'=>'Departments']) @endcomponent @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent