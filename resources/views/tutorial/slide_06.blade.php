@component('layouts.components.timeline_item',['i'=>$i,'color'=>'bg-aqua']) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <object class="img-responsive" type="image/svg+xml" data="{{url('/images/tutorial/06.svg')}}"></object>
    <br>
    @markdown @verbatim
    php -v @endverbatim @endmarkdown
    @markdown @verbatim
    composer -v @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
    <ul>
        <li>Link
            <ul>
                <li><a href="https://laragon.org/download">Laragon Full</a></li>
                <li><a href="https://tortoisegit.org/download">Tortoise GIT</a></li>
                <li><a href="https://about.gitlab.com">GITLAB Account</a></li>
            </ul>
        </li>
    </ul>
@endslot @endcomponent