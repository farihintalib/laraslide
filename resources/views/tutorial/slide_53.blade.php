@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code inside 'layouts\app.blade.php' after include('layouts.styles')</p>
    </div>
    @markdown @verbatim
    @stack('styles') @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'layouts\app.blade.php' after include('layouts.scripts')</p>
    </div>
    @markdown @verbatim
    @stack('scripts') @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'page\1.blade.php' in section 'main-content'</p>
    </div>
    @markdown @verbatim
    @push('scripts')
        <script>alert('this is page 1 line 16')</script>
    @endpush
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Title Page 1</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            Body Page 1
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            Footer Page 1
        </div>
        <!-- /.box-footer-->
    </div>
    @push('scripts')
        <script>alert('this is page 1 line 28')</script>
    @endpush @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent