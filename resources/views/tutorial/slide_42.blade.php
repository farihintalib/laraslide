@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code inside 'attendance/index.blade.php' after edit link</p>
    </div>
    @markdown @verbatim
    <form id="attendance{{$attendance['id']}}" method="POST" action="{{route('attendances.destroy',['attendance'=>$attendance['id']])}}">
        @csrf
        @method('DELETE')
        <a href="javascript:void()" onclick="document.getElementById('attendance{{$attendance['id']}}').submit()">Delete</a>
    </form> @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside AttendanceController@destroy</p>
    </div>
    @markdown @verbatim
    public function destroy($id) {
        $attendances = session('attendances');
        if (isset($attendances)) {
            $attendances = $attendances->filter(function ($value)use($id) {
                return $value['id'] != $id;
            });
            Request()->session()->put('attendances', $attendances);
            return redirect(route('attendances.index'));
        } else {
            return abort(404);
        }
    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent