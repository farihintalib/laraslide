@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Laravel includes a variety of global "helper" PHP functions.</p>
    </div>
    @markdown @verbatim
    public function show($name) {
        // return Str::camel("{$this->greeting()} {$name}");
        // return Str::slug("{$this->greeting()} {$name}");
        return Str::upper("{$this->greeting()} {$name}");
    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://laravel.com/docs/9.x/helpers">Helpers</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent