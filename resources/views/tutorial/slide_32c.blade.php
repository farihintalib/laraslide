@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Modify the code inside GreetingController@show</p>
    </div>
    @markdown @verbatim
    public function show($name) {
        $name = Str::upper("{$this->greeting()} {$name}");
        return response($name, 200)->header('Content-Type', 'text/plain');
    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://laravel.com/docs/9.x/responses">Full Documentation</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent