@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Do the same language for 'page/1' and 'page/3'</p>
    </div>
    <div class="callout callout-success">
        <p>Write this code inside route</p>
    </div>
    @markdown @verbatim
    Route::get('set_locale/{locale}', function ($locale) {
        session()->put('locale', $locale);
        return 'Set language ' . $locale;
    })->name('set_locale'); @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Comment this code inside route 'page/2'</p>
    </div>
    @markdown @verbatim
    App::setLocale(Request()->lang); @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside command</p>
    </div>
    @markdown @verbatim
    php artisan make:middleware Locale @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside 'Middleware/Locale.php' and register in 'Http/Kernal.php'</p>
    </div>
    @markdown @verbatim
    public function handle($request, Closure $next)
    {
        App::setLocale($request->session()->get('locale'));
        return $next($request);
    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent