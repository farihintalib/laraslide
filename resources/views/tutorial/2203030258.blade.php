@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Datatable</p>
    </div>
    @markdown @verbatim
    public function query(Attendance $model) {
        return $model->select('attendances.*')->with('attendanceLookupByStatusId');
    }
    
    return [
        Column::make('id')->width(60)->addClass('text-center'),
        Column::make('name'),
        Column::make('date'),
        Column::make('att_in'),
        Column::make('att_out'),
        Column::make('attendance_lookup_by_status_id.label')->name('attendanceLookupByStatusId.label')->title('Status'),
        Column::make('action'),
    ]; @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent