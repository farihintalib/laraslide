@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <object class="img-responsive" type="image/svg+xml" data="{{url('/images/tutorial/picture37.svg')}}"></object>
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://docs.spatie.be/laravel-permission/v3/installation-laravel/">Spatie Permissions</a></li>
                    <li><a href="https://github.com/yajra/laravel-datatables">Yajra Datatable</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent