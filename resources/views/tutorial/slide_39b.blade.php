@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Route</p>
    </div>
    @markdown @verbatim
    Route::get('uploads', [Controllers\UploadController::class, 'index'])->name('uploads.index');
    Route::post('uploads', [Controllers\UploadController::class, 'store'])->name('uploads.store'); @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>View</p>
    </div>
    @markdown @verbatim
    <form method="post" action="{{route('uploads.store')}}" enctype="multipart/form-data">
        @csrf
        <input name="file" type="file">
        <button type='submit'>Save</button>
    </form> @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Controller</p>
    </div>
    @markdown @verbatim
    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Storage;

    class UploadController extends Controller {

        public function index(Request $request) {
            return view('upload.index');
        }

        public function store(Request $request) {
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $name = $file->hashName();
                $extension = $file->extension();
                $path = $request->file('file')->storeAs('/', $name, 'public');
                $url = Storage::url($path);
            }
            dd($request->all(), url($url));
            return redirect()->back();
        }

    } @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://laravel.com/docs/9.x/filesystem">Full Documentation</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent