@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code inside 'attendance/index.blade.php' after view link</p>
    </div>
    @markdown @verbatim
    <a href="{{route('attendances.edit',['attendance'=>$attendance['id']])}}">Edit</a> @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside AttendanceController@edit</p>
    </div>
    @markdown @verbatim
    public function edit($id) {
        $attendances = session('attendances');
        if (isset($attendances)) {
            $attendance = $attendances->firstWhere('id', $id);
            return view('attendance.edit', compact('attendance'));
        } else {
            return abort(404);
        }
    } @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write this code inside AttendanceController@update</p>
    </div>
    @markdown @verbatim
    dd(Request(),Request()->all()); @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Copy 'welcome.blade.php' to 'views/attendance' and rename to 'edit.blade.php'. Write this code inside 'edit.blade.php'</p>
    </div>
    @markdown @verbatim
    <!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <title>Laravel</title>
            <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
            <style>
                html, body {
                    background-color: #fff;
                    color: #636b6f;
                    font-family: 'Nunito', sans-serif;
                    font-weight: 200;
                    height: 100vh;
                    margin: 0;
                }
                .full-height {
                    height: 100vh;
                }
                .flex-center {
                    align-items: center;
                    display: flex;
                    justify-content: center;
                }
                .position-ref {
                    position: relative;
                }
                .top-right {
                    position: absolute;
                    right: 10px;
                    top: 18px;
                }
                .content {
                    text-align: center;
                }
                .title {
                    font-size: 84px;
                }
                .links > a {
                    color: #636b6f;
                    padding: 0 25px;
                    font-size: 13px;
                    font-weight: 600;
                    letter-spacing: .1rem;
                    text-decoration: none;
                    text-transform: uppercase;
                }
                .m-b-md {
                    margin-bottom: 30px;
                }
            </style>
        </head>
        <body>
            <div class="flex-center position-ref full-height">
                @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                    <a href="{{ url('/home') }}">Home</a>
                    @else
                    <a href="{{ route('login') }}">Login</a>
                    @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                    @endif
                    @endauth
                </div>
                @endif
                <div class="content">
                    <div class="m-b-md">
                        <form method="POST" action="{{ route('attendances.update',['attendance'=>$attendance['id']]) }}">
                            @csrf
                            @method('PUT')
                            Id: <input type="number" name="id" value="{{$attendance['id']}}"> <br/>
                            Name: <input type="text" name="name" value="{{$attendance['name']}}"> <br/>
                            Date: <input type="text" name="date" value="{{$attendance['date']}}"> <br/>
                            Clock In: <input type="text" name="att_in" value="{{$attendance['att_in']}}"> <br/>
                            Clock Out: <input type="text" name="att_out" value="{{$attendance['att_out']}}"> <br/>
                            <button type="submit">Update</button>
                        </form>
                    </div>
                    <div class="links">
                        <a href="https://laravel.com/docs">Docs</a>
                        <a href="https://laracasts.com">Laracasts</a>
                        <a href="https://laravel-news.com">News</a>
                        <a href="https://blog.laravel.com">Blog</a>
                        <a href="{{url('tutorial')}}">Tutorial</a>
                        <a href="https://github.com/laravel/laravel">GitHub</a>
                    </div>
                </div>
            </div>
        </body>
    </html> @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent