@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write the code at route</p>
    </div>
    @markdown @verbatim
    Route::group([
    'prefix' => 'adminlte',
    'as' => 'adminlte.',
        ], function () {
            Route::get('index', [Controllers\AdminLTEController::class, 'index'])->name('index');
            Route::get('index2', [Controllers\AdminLTEController::class, 'index2'])->name('index2');
            Route::get('chartjs', [Controllers\AdminLTEController::class, 'chartjs'])->name('chartjs');
            Route::get('blank', [Controllers\AdminLTEController::class, 'blank'])->name('blank');
        }); @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Write the code at AdminLTEController</p>
    </div>
    @markdown @verbatim
    public function index() {
        return view('adminlte.index');
    }

    public function index2() {
        return view('adminlte.index2');
    } @endverbatim @endmarkdown
    <div class="callout callout-success">
        <p>Create the blade. Get the code for blade using reference given. View page source, copy & paste</p>
    </div>
    <div class="callout callout-success">
        <p>Copy the asset from public laraslide to public/adminlte laravel project</p>
    </div>
    <div class="callout callout-success">
        <p>Try figure out chartjs and blank page</p>
    </div>
@endslot @slot('timeline_footer')
    <div class='box-footer'>
        <ul>
            <li>Link
                <ul>
                    <li><a href="https://adminlte.io/themes/AdminLTE/index.html">index</a></li>
                    <li><a href="https://adminlte.io/themes/AdminLTE/index2.html">index2</a></li>
                    <li><a href="https://adminlte.io/themes/AdminLTE/pages/charts/chartjs.html">chartjs</a></li>
                    <li><a href="https://adminlte.io/themes/AdminLTE/pages/examples/blank.html">blank</a></li>
                </ul>
            </li>
        </ul>
    </div>
@endslot @endcomponent