@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code to welcome page and inspect</p>
    </div>
    @markdown @verbatim
    <meta name="csrf-token" content="{{ csrf_token() }}"> @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent