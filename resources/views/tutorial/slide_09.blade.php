@component('layouts.components.timeline_item',['i'=>$i,'color'=>'bg-aqua']) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <img class="img-responsive" src="{{url('/images/tutorial/picture6.jpg')}}">
    <br />
    @markdown @verbatim
    php artisan @endverbatim @endmarkdown
@endslot @slot('timeline_footer')

@endslot @endcomponent