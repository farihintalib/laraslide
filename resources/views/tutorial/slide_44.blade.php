@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Write this code inside 'create.blade.php'</p>
    </div>
    @markdown @verbatim
    Id: <input type="number" name="id" required min="0" max="10"> <br/>
    Name: <input type="text" name="name" required maxlength="10" placeholder="First name"> <br/>
    Date: <input type="date" name="date" min="2019-11-01"> <br/>
    Clock In: <input type="time" name="att_in"> <br/>
    Clock Out: <input type="time" name="att_out"> <br/> @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent