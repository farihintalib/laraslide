@extends('layouts.tutorial.app')

@section('main-content')
    <!-- The time line -->
    <ul class="timeline">
        <!-- timeline time label -->
        <li class="time-label">
            <span class="bg-red">
                24/02/2022
            </span>
        </li>
        @php $i = 1 @endphp
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'Profile','svgPath'=>url('/images/tutorial/01.svg')])
        @endcomponent
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'Overview on web based system development process','svgPath'=>url('/images/tutorial/84.svg')])
            @slot('link')
                <li><a href="{{url('document/Example_-_URS.docx')}}">Example_-_URS</a></li>
                <li><a href="{{url('document/Example_UAT_Test_Script.xlsx')}}">Example_UAT_Test_Script</a></li>
            @endslot
        @endcomponent
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'Prototype vs Development','svgPath'=>url('/images/tutorial/85.svg')])
        @endcomponent
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'My role and responsibility in a team','svgPath'=>url('/images/tutorial/86.svg')])
        @endcomponent
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'Developer','svgPath'=>url('/images/tutorial/87.svg')])
        @endcomponent
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'Teamwork using GIT tool: Branch Type','svgPath'=>url('/images/tutorial/88.svg')])
        @endcomponent
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'Teamwork using GIT tool: Git steps before making pull request','svgPath'=>url('/images/tutorial/89.svg')])
        @endcomponent
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'Outcome Course','svgPath'=>url('/images/tutorial/02.svg')])
        @endcomponent
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'Outline Course','svgPath'=>url('/images/tutorial/Picture39.svg')])
        @endcomponent
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'Laravel Introduction','svgPath'=>url('/images/tutorial/04.svg')])
            @slot('link')
                <li><a href="https://laravel.com">Laravel</a></li>
            @endslot
        @endcomponent
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'MVC?','svgPath'=>url('/images/tutorial/12.svg')])
        @endcomponent
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'What We Need?','svgPath'=>url('/images/tutorial/05.svg')])
        @endcomponent
        @include('tutorial.slide_06',['i'=>$i++,'title'=>'Requirement'])
        @include('tutorial.slide_07',['i'=>$i++,'title'=>'My First Laravel Project'])
        @include('tutorial.slide_88',['i'=>$i++,'title'=>'Composer JSON'])
        @include('tutorial.slide_08',['i'=>$i++,'title'=>'Local Deploy'])
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'Deploy Laravel Tutorial','svgPath'=>url('/images/tutorial/11.svg')])
        @endcomponent
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'Deploy From Git','svgPath'=>url('/images/tutorial/90.svg')])
        @endcomponent
        @include('tutorial.slide_09',['i'=>$i++,'title'=>'Artisan Command'])
        @include('tutorial.slide_10',['i'=>$i++,'title'=>'Scaffold With Artisan Make'])
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'Configuration','svgPath'=>url('/images/tutorial/91.svg')])
            @slot('link')
                <li><a href="https://laravel.com/docs/9.x/configuration">Full Documentation</a></li>
            @endslot
        @endcomponent
        @include('tutorial.slide_90',['i'=>$i++,'title'=>'Exercise: Config'])
        @include('tutorial.slide_13',['i'=>$i++,'title'=>'Architecture File/Folder'])
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'Scaffold Standard Name','svgPath'=>url('/images/tutorial/14.svg')])
        @endcomponent
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'Route','svgPath'=>url('/images/tutorial/15.svg')])
            @slot('link')
                <li><a href="https://laravel.com/docs/9.x/routing">Full Documentation</a></li>
            @endslot
        @endcomponent
        @include('tutorial.slide_16',['i'=>$i++,'title'=>'Exercise: Basic Route'])
        @include('tutorial.slide_17',['i'=>$i++,'title'=>'Exercise: Passing Data to Route'])
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'Accessing The Current Route','svgPath'=>url('/images/tutorial/18.svg')])
        @endcomponent
        @include('tutorial.slide_89',['i'=>$i++,'title'=>'Exercise: Route Group'])
        @include('tutorial.slide_19',['i'=>$i++,'title'=>'DD: Dump & Die'])
        @component('layouts.components.timeline_item_type1',['i'=>$i++,'color'=>'bg-aqua',
            'title'=>'Middleware','svgPath'=>url('/images/tutorial/20.svg')])
            @slot('link')
                <li><a href="https://laravel.com/docs/9.x/middleware">Full Documentation</a></li>
            @endslot
        @endcomponent
        @include('tutorial.slide_21',['i'=>$i++,'title'=>'Exercise: Middleware Auth'])
        @include('tutorial.slide_22',['i'=>$i++,'title'=>'CSRF Protection'])
        @include('tutorial.slide_23',['i'=>$i++,'title'=>'Exercise: CSRF Token'])
        @include('tutorial.slide_24',['i'=>$i++,'title'=>'Controller'])
        @include('tutorial.slide_25',['i'=>$i++,'title'=>'Exercise: Basic Controller'])
        <li class="time-label">
            <span class="bg-red">
                25/02/2022
            </span>
        </li>
        @include('tutorial.slide_25b',['i'=>$i++,'title'=>'Exercise: Basic Controller - Continue'])
        @include('tutorial.slide_25c',['i'=>$i++,'title'=>'Laravel Helper'])
        @include('tutorial.slide_25d',['i'=>$i++,'title'=>'Exercise: Middleware'])
        @include('tutorial.slide_27',['i'=>$i++,'title'=>'Resource Controller for CRUD'])
        @include('tutorial.slide_26',['i'=>$i++,'title'=>'Exercise: Resource Controller'])
        @include('tutorial.slide_28',['i'=>$i++,'title'=>'Request'])
        @include('tutorial.slide_29',['i'=>$i++,'title'=>'Exercise: Basic Request'])
        @include('tutorial.slide_30',['i'=>$i++,'title'=>'Responses'])
        @include('tutorial.slide_31',['i'=>$i++,'title'=>'View'])
        @include('tutorial.slide_32',['i'=>$i++,'title'=>'Exercise: View'])
        @include('tutorial.slide_32e',['i'=>$i++,'title'=>'Exercise: View AdminLte'])
        @include('tutorial.slide_32c',['i'=>$i++,'title'=>'Exercise: JSON'])
        @include('tutorial.slide_32b',['i'=>$i++,'title'=>'Exercise: Redirect'])
        <li class="time-label">
            <span class="bg-red">
                1/03/2022
            </span>
        </li>
        @include('tutorial.slide_32d',['i'=>$i++,'title'=>'Exercise: File'])
        @include('tutorial.slide_33',['i'=>$i++,'title'=>'Exercise: Basic Display a List'])
        @include('tutorial.slide_34',['i'=>$i++,'title'=>'Exercise: Display a List from Controller'])
        @include('tutorial.2202281455',['i'=>$i++,'title'=>'Exercise: Display a List using Yajra Datatable'])
        @include('tutorial.slide_35',['i'=>$i++,'title'=>'Exercise: Display a Show from List'])
        @include('tutorial.slide_36',['i'=>$i++,'title'=>'Exercise: Display a Create Form'])
        @include('tutorial.slide_40',['i'=>$i++,'title'=>'Session'])
        @include('tutorial.slide_37',['i'=>$i++,'title'=>'Exercise: Store Data in Session'])
        @include('tutorial.slide_39',['i'=>$i++,'title'=>'Form'])
        @include('tutorial.slide_39b',['i'=>$i++,'title'=>'Exercise: Upload File'])
        @include('tutorial.slide_39c',['i'=>$i++,'title'=>'Exercise: Upload Excel'])
        @include('tutorial.slide_38',['i'=>$i++,'title'=>'Exercise: Display a Edit Form'])
        @include('tutorial.slide_41',['i'=>$i++,'title'=>'Exercise: Update Data in Session'])
        @include('tutorial.slide_42',['i'=>$i++,'title'=>'Exercise: Delete Data in Session'])
        @include('tutorial.2202280811',['i'=>$i++,'title'=>'Exercise: Display Apexcharts'])
        @include('tutorial.2202281414',['i'=>$i++,'title'=>'Exercise: Display Apexcharts from Controller'])
        @include('tutorial.slide_43',['i'=>$i++,'title'=>'Validation'])
        @include('tutorial.slide_44',['i'=>$i++,'title'=>'Exercise: Frontend Validation'])
        @include('tutorial.slide_45',['i'=>$i++,'title'=>'Exercise: Backend Validation'])
        @include('tutorial.slide_46',['i'=>$i++,'title'=>'Error Handling & Logging'])
        <li class="time-label">
            <span class="bg-red">
                2/03/2022
            </span>
        </li>
        @include('tutorial.slide_47',['i'=>$i++,'title'=>'AdminLTE Panel'])
        @include('tutorial.slide_48',['i'=>$i++,'title'=>'Blade Templates 1'])
        @include('tutorial.slide_49',['i'=>$i++,'title'=>'Exercise: Multiple Page using AdminLte'])
        @include('tutorial.slide_50',['i'=>$i++,'title'=>'Blade Templates 2'])
        @include('tutorial.slide_51',['i'=>$i++,'title'=>'Exercise: Component & Slot'])
        @include('tutorial.2203011417',['i'=>$i++,'title'=>'Exercise: X-blade'])
        @include('tutorial.slide_52',['i'=>$i++,'title'=>'Blade Templates 3'])
        @include('tutorial.slide_53',['i'=>$i++,'title'=>'Exercise: Stack & Push'])
        @include('tutorial.slide_54',['i'=>$i++,'title'=>'Localization'])
        @include('tutorial.slide_55',['i'=>$i++,'title'=>'Exercise: Localization'])
        @include('tutorial.slide_56',['i'=>$i++,'title'=>'Exercise: Set Localization'])
        @include('tutorial.slide_58',['i'=>$i++,'title'=>'Session & Middleware'])
        @include('tutorial.slide_57',['i'=>$i++,'title'=>'Exercise: Set Localization in Session'])
        @include('tutorial.slide_59',['i'=>$i++,'title'=>'Authentication'])
        @include('tutorial.slide_60',['i'=>$i++,'title'=>'Exercise: Scaffold Authentication for Laravel'])
        @include('tutorial.slide_61',['i'=>$i++,'title'=>'Exercise: Change Login, Register, Forget Password & Home Page using AdminLTE'])
        @include('tutorial.slide_62',['i'=>$i++,'title'=>'Model'])
        @include('tutorial.slide_64',['i'=>$i++,'title'=>'Database'])
        @include('tutorial.slide_65',['i'=>$i++,'title'=>'Exercise: Interact with Database'])
        @include('tutorial.slide_63',['i'=>$i++,'title'=>'Eloquent ORM'])
        @include('tutorial.2203012247',['i'=>$i++,'title'=>'Exercise: Save Attendance in DB'])
        <li class="time-label">
            <span class="bg-red">
                3/03/2022
            </span>
        </li>
        @include('tutorial.2203030202',['i'=>$i++,'title'=>'Exercise: Soft Delete'])
        @include('tutorial.2203030210',['i'=>$i++,'title'=>'Exercise: Query Builder'])
        @include('tutorial.2203012343',['i'=>$i++,'title'=>'Exercise: List Data using Datatable Query'])
        @include('tutorial.2203030245',['i'=>$i++,'title'=>'Exercise: BelongsTo'])
        @include('tutorial.2203030258',['i'=>$i++,'title'=>'Exercise: BelongsTo in Datatable Query'])
        
        @include('tutorial.2203030322',['i'=>$i++,'title'=>'Exercise: Seeder'])
        
        @include('tutorial.2203020007',['i'=>$i++,'title'=>'Exercise: Import Excel to DB'])
        @include('tutorial.2203020019',['i'=>$i++,'title'=>'Exercise: Statistic Chart using Query'])
        @include('tutorial.slide_66',['i'=>$i++,'title'=>'Exercise: Login Register'])
        @include('tutorial.2203030303',['i'=>$i++,'title'=>'Exercise: Mutator'])
        @include('tutorial.2203030318',['i'=>$i++,'title'=>'Laravel Auditing'])
        @include('tutorial.slide_67',['i'=>$i++,'title'=>'Exercise: Profile'])
        @include('tutorial.slide_68',['i'=>$i++,'title'=>'Exercise: Display Users from Model'])
        @include('tutorial.slide_69',['i'=>$i++,'title'=>'Exercise: Show User from Model'])
        @include('tutorial.slide_70',['i'=>$i++,'title'=>'Exercise: Create User Form'])
        @include('tutorial.slide_71',['i'=>$i++,'title'=>'Exercise: Store User to Model'])
        @include('tutorial.slide_72',['i'=>$i++,'title'=>'Exercise: Edit User Form'])
        @include('tutorial.slide_73',['i'=>$i++,'title'=>'Exercise: Update User to Model'])
        @include('tutorial.slide_74',['i'=>$i++,'title'=>'Exercise: Delete User from Model'])
        @include('tutorial.slide_75',['i'=>$i++,'title'=>'Exercise: Return Message from Controller'])
        @include('tutorial.slide_76',['i'=>$i++,'title'=>'Exercise: DIY Department Module'])
        @include('tutorial.slide_77',['i'=>$i++,'title'=>'Exercise: User Department'])
        @include('tutorial.slide_78',['i'=>$i++,'title'=>'Exercise: Department List User'])
        @include('tutorial.slide_79',['i'=>$i++,'title'=>'Exercise: User Birthday'])
        @include('tutorial.2203030331',['i'=>$i++,'title'=>'Test'])
        @include('tutorial.slide_80',['i'=>$i++,'title'=>'Step for CRUD'])
        @include('tutorial.slide_81',['i'=>$i++,'title'=>'Package'])
        <li>
            <i class="fa fa-clock-o bg-gray"></i>
        </li>
    </ul>
@endsection