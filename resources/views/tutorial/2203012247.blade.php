@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>CLI</p>
    </div>
    @markdown @verbatim
    php artisan make:migration create_attendances_table @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Migration</p>
    </div>
    
    @markdown @verbatim
    Schema::create('attendances', function (Blueprint $table) {
        $table->id();
        $table->string('name');
        $table->date('date');
        $table->time('att_in')->nullable();
        $table->time('att_out')->nullable();
        $table->enum('status', ['late','absent','present'])->default('present');
        $table->timestamps();
    }); @endverbatim @endmarkdown
     
    <div class="callout callout-success">
        <p>CLI</p>
    </div>
    @markdown @verbatim
    php artisan migrate @endverbatim @endmarkdown
    @markdown @verbatim
    php artisan make:model Attendance @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Model</p>
    </div>
    
    @markdown @verbatim
    protected $fillable = [
        'name',
        'date',
        'att_in',
        'att_out',
    ]; @endverbatim @endmarkdown
    
    <div class="callout callout-success">
        <p>Controller</p>
    </div>
    
    @markdown @verbatim
    use App\Models\Attendance;
    use Carbon\Carbon; 
    
    public function index(Request $request) {
        $attendances = Attendance::all();
        return view('attendance.index', compact('attendances'));
    }
    
    public function store(UpdateRequest $request) {
        $attendance = new Attendance();
        $attendance->name = $request->name;
        $attendance->date = Carbon::parse($request->date);
        $attendance->att_in = Carbon::parse("{$request->date} {$request->att_in}");
        $attendance->att_out = Carbon::parse("{$request->date} {$request->att_out}");
        if ($attendance->att_in->gte(Carbon::parse('today 8am'))) {
            $attendance->status = 'late';
        }
        $attendance->save();
        return redirect(route('attendances.index'));
    }
    
    public function show($id) {
        $attendance = Attendance::findOrFail($id);
        return view('attendance.show', compact('attendance'));
    }
    
    public function edit($id) {
        $attendance = Attendance::findOrFail($id);
        return view('attendance.edit', compact('attendance'));
    }
    
    public function update(UpdateRequest $request, $id) {
        $attendance = Attendance::findOrFail($id);
        $attendance->name = $request->name;
        $attendance->date = Carbon::parse($request->date);
        $attendance->att_in = Carbon::parse("{$request->date} {$request->att_in}");
        $attendance->att_out = Carbon::parse("{$request->date} {$request->att_out}");
        if ($attendance->att_in->gte(Carbon::parse('today 8am'))) {
            $attendance->status = 'late';
        }
        $attendance->save();
        return redirect(route('attendances.index'));
    } 
    
    public function destroy($id) {
        $attendance = Attendance::findOrFail($id);
        $attendance->delete();
        return redirect(route('attendances.index'));
    } @endverbatim @endmarkdown
     
@endslot @slot('timeline_footer')
@endslot @endcomponent