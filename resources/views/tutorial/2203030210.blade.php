@component('layouts.components.timeline_item',['color'=>'bg-aqua','i'=>$i ?? 1]) @slot('timeline_header')
    <a href='#'>{{$title ?? null}}</a>
@endslot @slot('timeline_body')
    <div class="callout callout-success">
        <p>Controller</p>
    </div>
    @markdown @verbatim
    use Illuminate\Support\Facades\DB;
    
    $attendances = DB::table('attendances')->get();
    $attendances = Attendance::all(); @endverbatim @endmarkdown
@endslot @slot('timeline_footer')
@endslot @endcomponent