@extends('layouts.tutorial.app')

@section('main-content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Profile</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <p><strong>Name:</strong> {{Auth::user()->name}}<br><strong>Email:</strong> {{Auth::user()->email}}</p>
        </div>
    </div>
@endsection  
