<!-- Generate at 2021-03-28 02:41:06. Clear comment if this file being used -->
<div class="form-group">
    <label for="first_name">First Name <span class="text-danger">*</span></label>
    <input id="first_name" required name="first_name" data-label="first_name" type="text" class="form-control">
</div>
<div class="form-group">
    <label for="last_name">Last Name</label>
    <input id="last_name" name="last_name" data-label="last_name" type="text" class="form-control">
</div>
<div class="form-group">
    <label for="email">Email</label>
    <input id="email" name="email" data-label="email" type="email" class="form-control">
</div>
<div class="form-group">
    <label for="company_id">Company<span class="text-danger">*</span></label>
    <select id="company_id" name="company_id" data-label="company_id" class="form-control">
        <option value="">-- Please Select --</option>
        @foreach($companies as $key => $value)
            <option value="{{$key}}">{{$value}}</option>
        @endforeach
    </select>
</div>