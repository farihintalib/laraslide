@extends('layouts.app')

<!-- Generate at 2021-03-28 02:41:06. Clear comment if this file being used -->

@section('main-content')
    @component('layouts.components.modal_edit',['id'=>'employeeDtStore',
        'route'=>route('employees.store'),'title'=>__('Create')])
        @slot('html')
            @include('employee.form.form')
        @endslot
    @endcomponent
    @component('layouts.components.modal_edit',['id'=>'employeeDtUpdate',
        'route'=>route('employees.update','%24%7Bdata.id%7D'),'title'=>__('Edit')])
        @slot('html')
            @method('PUT')
            @include('employee.form.form')
        @endslot
        @slot('onOpen')
            @include('employee.form.form_script',['id'=>'employeeDtUpdate'])
        @endslot
    @endcomponent
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{!! __('employee') !!}</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div>
                Total Record: <span id="result"></span><br>
                Filter First name: <input id="first_name" type="text"/><br>
                Filter Last name: <input id="last_name" type="text"/><br>
                Filter Email: <input id="email" type="text"/><br>
                <button onclick='window.LaravelDataTables["employeeDt"].ajax.reload()'>Filter</button>
            </div>
            @include('layouts.components.session_message')
            {!! $employeeDt->html()->table(['class' => 'table table-bordered table-striped']) !!}
            @push('scripts')
                {!! $employeeDt->html()->scripts() !!}
            @endpush
        </div>
    </div>
@endsection