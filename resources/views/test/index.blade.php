@extends('layouts.app')

<!-- Generate at 2021-03-26 17:11:12. Clear comment if this file being used -->

@section('main-content')
    @component('layouts.components.modal_edit',['id'=>'testDtStore',
        'route'=>route('tests.store'),'title'=>__('Create')])
        @slot('html')
            @include('test.form.form')
        @endslot
    @endcomponent
    @component('layouts.components.modal_edit',['id'=>'testDtUpdate',
        'route'=>route('tests.update','%24%7Bdata.id%7D'),'title'=>__('Edit')])
        @slot('html')
            @method('PUT')
            @include('test.form.form')
        @endslot
        @slot('onOpen')
            @include('test.form.form_script',['id'=>'testDtUpdate'])
        @endslot
    @endcomponent
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{!! __('test') !!}</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            @include('layouts.components.session_message')
            {!! $testDt->html()->table(['class' => 'table table-bordered table-striped']) !!}
            @push('scripts')
                {!! $testDt->html()->scripts() !!}
            @endpush
        </div>
    </div>
@endsection