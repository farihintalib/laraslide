<!-- Generate at 2021-03-28 02:40:12. Clear comment if this file being used -->
<div class="form-group">
    <label for="name">Name<span class="text-danger">*</span></label>
    <input id="name" name="name" required data-label="name" type="text" class="form-control">
</div>
<div class="form-group">
    <label for="email">Email</label>
    <input id="email" name="email" data-label="email" type="email" class="form-control">
</div>