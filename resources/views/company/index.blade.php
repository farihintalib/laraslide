@extends('layouts.app')

<!-- Generate at 2021-03-28 02:40:12. Clear comment if this file being used -->

@section('main-content')
    @component('layouts.components.modal_edit',['id'=>'companyDtStore',
        'route'=>route('companies.store'),'title'=>__('Create')])
        @slot('html')
            @include('company.form.form')
        @endslot
    @endcomponent
    @component('layouts.components.modal_edit',['id'=>'companyDtUpdate',
        'route'=>route('companies.update','%24%7Bdata.id%7D'),'title'=>__('Edit')])
        @slot('html')
            @method('PUT')
            @include('company.form.form')
        @endslot
        @slot('onOpen')
            @include('company.form.form_script',['id'=>'companyDtUpdate'])
        @endslot
    @endcomponent
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{!! __('company') !!}</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            @include('layouts.components.session_message')
            {!! $companyDt->html()->table(['class' => 'table table-bordered table-striped']) !!}
            @push('scripts')
                {!! $companyDt->html()->scripts() !!}
            @endpush
        </div>
    </div>
@endsection