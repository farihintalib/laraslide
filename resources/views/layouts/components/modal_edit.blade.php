@push('script')
    var {{$id}}Settings = {
        title: '{{$title ?? null}}',
        icon: '{{$icon ?? null}}',
        html: `<form id="{{$id}}">{!! $html ?? null !!}</form>`,
        showCancelButton: {{$setCancelBtn ?? 'true'}},
        showConfirmButton: {{$setConfirmBtn ?? 'true'}},
        confirmButtonText: '{!! isset($confirmButtonText) ? $confirmButtonText : '<i class="fa fa-save"></i> Save' !!}',
        customClass: {
            popup: '{!! $class ?? null !!}',
        },
        axios:{
            headersAccept: '{{$accept ?? null}}',
            url: (data) => {
                return `{{urldecode($route ?? '')}}`;
            },
            afterValidate: (data) => {
                {!! $afterValidate ?? null !!}
            },
            afterSuccessResponse: (data) => {
                {!! $afterSuccessResponse ?? null !!}
            }
        },
        onOpen: (data) => {
            {!! $onOpen ?? null !!}
        },
    };
@endpush
