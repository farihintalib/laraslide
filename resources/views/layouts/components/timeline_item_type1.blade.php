<li>
    <i class="fa {{$color ?? 'bg-blue'}}">{{$i ?? 1}}</i>
    <div class="timeline-item">
        <h3 class="timeline-header">
            <a href='#'>{{$title ?? null}}</a>
        </h3>
        <div class="timeline-body">
            <object class="img-responsive" type="image/svg+xml" data="{{$svgPath}}"></object>
        </div>
        <div class="timeline-footer">
            <div class='box-footer'>
                @if(isset($link))
                    <ul>
                        <li>Link
                            <ul>
                                {!! $link ?? null !!}
                            </ul>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
    </div>
</li>