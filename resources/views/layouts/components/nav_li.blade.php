<li><a href="{{ $url ?? null}}">
        <i class="{{ $class ?? null}}"></i> 
        <span>{{ $label ?? null}}</span></a>
</li>