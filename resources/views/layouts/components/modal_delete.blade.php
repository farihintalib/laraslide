<div id="modal-delete" class="modal fade" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="">
                @csrf
                @method('DELETE')
                <div class="modal-header bg-red">
                    <button aria-label="Close" data-dismiss="modal" class="close" type="button">
                        <span aria-hidden="true"> x </span></button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure to delete? Proceed ?</p>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default pull-left" type="button">Close</button>
                    <button id="deleteButton" class="btn btn-danger" type="submit">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $(document).on("click", "a[data-modal]", function(e){
            e.preventDefault();
            var route = $(this).data("route");
            $("#modal-delete form").attr("action", route); //id hash, div/form/p tag
        });
    </script>
@endpush
