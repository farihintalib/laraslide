<li>
    <i class="fa {{$color ?? 'bg-blue'}}">{{$i ?? 1}}</i>
    <div class="timeline-item">
        <h3 class="timeline-header">
            {!! $timeline_header ?? null !!}
        </h3>
        <div class="timeline-body">
            {!! $timeline_body ?? null !!}
        </div>
        <div class="timeline-footer">
            {!! $timeline_footer ?? null !!}
        </div>
    </div>
</li>