<label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary" style="right: 0px;">
    <input class="{{$data['cbx']['checkboxId']}}-all" onclick="updateAllCheckboxesDt($(this),'input:checkbox.{{$data['cbx']['checkboxId']}}-single',{{json_encode($data['cbx'])}})" type="checkbox" value="true">&nbsp;<span class="bg-white"></span>{!! $data['cbx']['label'] !!}
</label>
<input id="{{$data['cbx']['checkboxId']}}" name="{{$data['cbx']['checkboxId']}}" type="hidden">
<input id="{{$data['cbx']['checkboxId']}}Name" name="{{$data['cbx']['checkboxId']}}Name" type="hidden">