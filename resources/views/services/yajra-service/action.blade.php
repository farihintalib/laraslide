<div class="btn-group">
    @if(isset($data['detail']) && $data['detail'] != [])
        <a class="btn btn-info btn-sm" href="{{url($data['detail']['route'])}}" title="Show"><i class="fa fa-eye"></i></a>
    @endif
    @if(isset($data['show']) && $data['show'] != [])
        <button class="btn btn-info btn-sm" type="button" title="Show" onclick="upsert({{json_encode($data['show'])}})"><i class="fa fa-eye"></i></button>
    @endif
    @if(isset($data['edit']) && $data['edit'] != [])
        <button class="btn btn-success btn-sm" type="button" title="Edit" onclick="upsert({{json_encode($data['edit'])}})"><i class="fa fa-edit"></i></button>
    @endif
    @if(isset($data['delete']) && $data['delete'] != [] && $data['delete']['route'] != '')
        <button class="btn btn-danger btn-sm" type="button" title="Delete" onclick="proceed({{json_encode($data['delete'])}})"><i class="fa fa-trash"></i></button>
    @elseif(isset($data['delete']) && $data['delete'] != [] && $data['delete']['route'] == '')
        <button disabled="" class="btn btn-danger btn-sm" type="button" title="{{$data['delete']['message']}}"><i class="fa fa-trash"></i></button>
    @endif
</div>