<i class="fas fa-ellipsis-v"></i>
<div class="dropdown-menu dropdown-menu-right">
    <a class="btn dropdown-item d-block text-danger" onclick="upsert({{json_encode($data['delete'])}})" href="#"><i class="fas fa-trash"></i>&nbsp;&nbsp;Delete Selected Record(s)</a>
</div>