<div class="btn btn-sm px-0">
    <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary" style="right: 0px;">
        <input class="{{$data['cbx']['checkboxId'] ?? null}}-single" {{$check ?? null}} onchange="updateCheckboxesDt($(this),{{json_encode($data['cbx'])}})" data-id="{{$data['cbx']['checkboxValue']}}" data-name="{{$data['cbx']['checkboxName'] ?? null}}" type="checkbox" value="true">&nbsp;<span class="bg-white"></span>
    </label>
</div>

