<?php

return [
    'title_1' => 'Title Page 1',
    'body_1' => 'Body Page 1',
    'footer_1' => 'Footer Page 1',
    
    'title_2' => 'Title Page 2',
    'body_2' => 'Body Page 2',
    'footer_2' => 'Footer Page 2',
    
    'title_3' => 'Title Page 3',
    'body_3' => 'Body Page 3',
    'footer_3' => 'Footer Page 3',
];  
