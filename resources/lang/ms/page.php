<?php

return [
    'title_1' => 'Tajuk Mukasurat 1',
    'body_1' => 'Kandungan Mukasurat 1',
    'footer_1' => 'Bawah Mukasurat 1',
    
    'title_2' => 'Tajuk Mukasurat 2',
    'body_2' => 'Kandungan Mukasurat 2',
    'footer_2' => 'Bawah Mukasurat 2',
    
    'title_3' => 'Tajuk Mukasurat 3',
    'body_3' => 'Kandungan Mukasurat 3',
    'footer_3' => 'Bawah Mukasurat 3',
];  
