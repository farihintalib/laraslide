<?php

/**
 * Generate at 2021-03-26 14:29:28. Clear comment if this file being used
 */

namespace App\Repositories;

use App\Models\Test;
use App\Repositories\BaseRepository;

class TestRepository extends BaseRepository {

    protected $model;

    public function __construct() {
        $this->model = new Test();
    }
    
    public function queryAll() {
        return $this->with([]);
    } 

}