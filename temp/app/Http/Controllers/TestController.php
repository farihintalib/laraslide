<?php

/**
 * Generate at 2021-03-26 14:29:28. Clear comment if this file being used
 */

namespace App\Http\Controllers;

use App\Services\TestService;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    
    protected $testService;
    
    public function __construct() {
        $this->testService = new TestService();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->testService->index();
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return $this->testService->store();
    }

    /**
     * Update the specified resource in storage.
     * @param type $id
     * @return response
     */
    public function update($id)
    {
        return $this->testService->update($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param type $id
     * @return response
     */
    public function destroy($id)
    {
        return $this->testService->delete($id);
    }
    
    public function destroyAll()
    {
        return $this->testService->deleteAll();
    }
}
