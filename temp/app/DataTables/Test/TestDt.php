<?php

/**
 * Generate at 2021-03-26 14:29:28. Clear comment if this file being used
 */

namespace App\DataTables\Test;

use App\Services\TestService;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class TestDt extends DataTable
{
    protected $tableId;
    protected $checkboxId;
    protected $testService;
    
    public function __construct() {
        $this->tableId = 'testDt';
        $this->testService = new TestService();
        $this->checkboxId = $this->tableId . 'Cbx';
    }
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('checkbox', function($query) {
                $param = [
                    'checkboxId' => $this->checkboxId,
                    'passDataTo' => $this->tableId . 'More',
                    'checkboxFn' => null,
                ];
                return $this->testService->testDtCheckbox($query,$param);
            })
            ->addColumn('action', function($query){
                return $this->testService->testDtAction($query);
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['checkbox','action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \DummyModel $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return $this->testService->testDt();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $data = [
            'table' => '"' . $this->tableId . '"',
            $this->checkboxId => $this->testService->yajraService->splitCheckboxValue($this->checkboxId),
        ];
        $appendData = $this->testService->yajraService->makeDataScript($data);
        return $this->builder()
                    ->setTableId($this->tableId)
                    ->columns($this->getColumns())
                    ->minifiedAjax('', $appendData)
                    ->dom("<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-4'f><'col-sm-12 col-md-4 dataTables_buttons'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>")
                    ->parameters([
                        'buttons' => [
                            $this->testService->yajraService->btnAdd(['swalId' => 'testDtStore']),
                            $this->testService->yajraService->btnMore([
                                    'tableId' => $this->tableId,
                                    'delete' => ['swalId' => 'testDtDeleteAll']
                                ]),
                        ],
                        'order' => [
                            3, 'asc'
                        ],
                        'responsive' => true,
                        'searching' => true,
                        'autoWidth' => false,
                        'drawCallback' => 'function () {
                            $(".'.$this->checkboxId.'-all").prop("checked",false);
                        }',
        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $params = [
            'checkboxId' => $this->checkboxId,
            'passDataTo' => $this->tableId . 'More',
            'label' => ''
        ];
        $checkboxAllView = $this->testService->yajraService->checkboxAllView($params);
        return [
            Column::computed('control','')->exportable(false)->printable(false)->width(20)->addClass('control'),
            Column::computed('checkbox', $checkboxAllView)->titleAttr('')->exportable(false)->printable(false)->width(20),
            Column::computed('DT_RowIndex', '#')->exportable(false)->printable(false)->width(20)->addClass('text-center'),
            Column::make('name'),
            Column::computed('action')->exportable(false)->printable(false)->width(80)->addClass('text-center all'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Test_' . date('YmdHis');
    }
}
