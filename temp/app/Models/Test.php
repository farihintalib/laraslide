<?php

/**
 * Generate at 2021-03-26 14:29:28. Clear comment if this file being used
 */

namespace App\Models;

use App\Models\BaseModel;
use App\Traits\Scopes\TestScope;

class Test extends BaseModel
{
    use TestScope;
    
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $primaryOrCompositeKey = ['id'];
    protected $table = 'tests';
    protected $fillable = [
        'name'
    ];
    
    /* =========================================================================
     * Relationship
     * =========================================================================
     */
    
    /* =========================================================================
     * Mutator
     * =========================================================================
     */
}
