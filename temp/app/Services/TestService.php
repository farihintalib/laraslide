<?php

/**
 * Generate at 2021-03-26 14:29:28. Clear comment if this file being used
 */

namespace App\Services;

use App\DataTables\Test\TestDt;
use App\Http\Requests\Test\StoreRequest;
use App\Http\Requests\Test\UpdateRequest;
use App\Services\BaseService;
use App\Repositories\TestRepository;
use Illuminate\Support\Arr;

class TestService extends BaseService {
    
    protected $testRepository;

    public function __construct() {
        parent::__construct();
        $this->testRepository = new TestRepository();
    }
    
    /**
     * https://laravel.com/docs/8.x/validation
     */
    public function dataType(array $validate) {
        $validation = [
            'id' => ['required','digits_between:1,11','integer'],
            'name' => ['max:50'],
            
        ];
        return Arr::only($validation, $validate);
    }
    
    
    public function index() {
        $title = __('test');
        if (!empty(request()->get('table'))) {
            return $this->dataTable();
        }
        $compact = array_merge($this->dataTable(), compact('title'));
        return view('test.index', $compact);
    }
    
    public function dataTable() {
        $testDt = new TestDt();
        if (request()->get('table') == 'testDt') {
            return $testDt->render(null);
        }
        return compact('testDt');
    }
    
    public function store() {
        $this->validateRequest(new StoreRequest());
        $input = request()->all();
        $input['created_by'] = auth()->user()->id ?? null;
        return $this->responseStore($this->testRepository->upsertByKey($input));
    }

    public function update($id) {
        $this->validateRequest(new UpdateRequest());
        $input = request()->all();
        $input['updated_by'] = auth()->user()->id ?? null;
        return $this->responseUpdate($this->testRepository->upsertByKey($input, $id));
    }

    public function delete($id) {
        return $this->responseDelete($this->testRepository->deleteByKey($id));
    }
    
    public function deleteAll() {
        foreach (explode(',', request()->testDtCbx) as $id) {
            $this->testRepository->deleteByKey($id);
        }
        return $this->responseDelete(true);
    }
    
    /* =========================================================================
     * DataTable
     * =========================================================================
     */
    public function testDt() {
        return $this->testRepository->queryAll();
    }
    
    public function testDtAction($query) {
        $id = $this->testRepository->getKey($query);
        $data = [
            'edit' => [
                'swalId' => 'testDtUpdate',
                'id' => $id,
                'name' => $query->name,
            ],
            'delete' => [
                'swalId' => 'testDtDeleteAll',
                'method' => 'delete',
                'title' => __('are you sure'),
                'message' => __('you wont be able to revert this'),
                'route' => route('tests.destroy',[$id]),
            ],
        ];
        return $this->yajraService->actionView($query, $data);
    }
    
    public function testDtCheckbox($query,$param) {
        $id = $this->testRepository->getKey($query);
        $param['checkboxValue'] = $id;
        $param['checkboxName'] = $query->name ?? null;
        return $this->yajraService->checkboxSingleView($param);
    }
    
    /* =========================================================================
     * Option Ajax
     * =========================================================================
     */

}
