@extends('layouts.app.index')

<!-- Generate at 2021-03-26 14:29:29. Clear comment if this file being used -->

@section('content')
    @component('layouts.components.modal_edit',['id'=>'testDtStore',
        'route'=>route('tests.store'),'title'=>__('Create')])
        @slot('html')
            @include('test.form.form')
        @endslot
    @endcomponent
    @component('layouts.components.modal_edit',['id'=>'testDtUpdate',
        'route'=>route('tests.update','%24%7Bdata.id%7D'),'title'=>__('Edit')])
        @slot('html')
            @method('PUT')
            @include('test.form.form')
        @endslot
        @slot('onOpen')
            @include('test.form.form_script',['id'=>'testDtUpdate'])
        @endslot
    @endcomponent
    @component('layouts.components.modal_edit',['id'=>'testDtDeleteAll',
        'route'=>route('tests.destroyAll'),'title'=>__("You've selected multiple channels to be deleted. Are you sure?"),
        'confirmButtonText'=>'Delete','icon'=>'warning'])
        @slot('html')
            @method('DELETE')
            <div class="text-center">You won't be able to revert this!</div>
            <input type="hidden" name="testDtCbx">
        @endslot
        @slot('onOpen')
            $("form#testDtDeleteAll [name='testDtCbx']").val($("#testDtCbx[name='testDtCbx']").val()).change();
        @endslot
        @slot('afterSuccessResponse')
            $("#testDtMore.buttons-more").hide();
            $("#testDtCbx[name='testDtCbx']").val('');
            $("#testDtCbxName[name='testDtCbxName']").val('');
        @endslot
    @endcomponent

    <div class="d-flex flex-column-fluid">
        <div class="container-fluid">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#testTab">
                        <span class="nav-icon"><i class="fas fa-bars"></i></span>
                        <span class="nav-text">{!! __('test') !!}</span>
                    </a>
                </li>
            </ul>
            <div class="card card-primary card-tabs">
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="testTab">
                            {!! $testDt->html()->table(['class' => 'table table-bordered table-striped']) !!}
                            @push('scripts')
                                {!! $testDt->html()->scripts() !!}
                            @endpush
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection